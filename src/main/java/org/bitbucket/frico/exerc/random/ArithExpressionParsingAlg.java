package org.bitbucket.frico.exerc.random;

import org.bitbucket.frico.ds.Stack;
import org.bitbucket.frico.ds.impl.LinkedListStack;

/**
 * Created by feliperico on 22/10/15.
 */
public class ArithExpressionParsingAlg {

    private static final String LEFT_PARENTHESIS = "(";
    private static final String RIGHT_PARENTHESIS = ")";

    private enum Operator {
        SUM("+"),
        SUB("-"),
        MULT("*"),
        DIV("/");

        String operatorStr;

        Operator(String operatorStr) {
            this.operatorStr = operatorStr;
        }

        static boolean isOperator(String operatorStr) {
            for (Operator oper : Operator.values()) {
                if (oper.operatorStr.equals(operatorStr)) {
                    return true;
                }
            }
            return false;
        }

        static Operator fromString(String operatorStr) {
            if (operatorStr != null) {
                operatorStr = operatorStr.trim();
                for (Operator oper : Operator.values()) {
                    if (oper.operatorStr.equals(operatorStr)) {
                        return oper;
                    }
                }
            }
            return null;
        }

        Double calculate(Double operand1, Double operand2) {
            if (operand1 != null && operand2 != null) {
                switch (this) {
                    case SUM:
                        return operand1 + operand2;
                    case SUB:
                        return operand1 - operand2;
                    case MULT:
                        return operand1 * operand2;
                    case DIV:
                        return operand1 / operand2;
                }
            }
            return null;
        }
    }

    public static double calculateResult(String expression) {
        Stack<Double> operandsStack = new LinkedListStack<Double>();
        Stack<Operator> operatorsStack = new LinkedListStack<Operator>();

        for (String token : getTokens(expression)) {
            if (!token.equals(LEFT_PARENTHESIS)) {
                if (Operator.isOperator(token)) {
                    operatorsStack.push(Operator.fromString(token));
                } else if (token.equals(RIGHT_PARENTHESIS)) {
                    Operator oper = operatorsStack.pop();
                    Double operand2 = operandsStack.pop();
                    Double operand1 = operandsStack.pop();
                    operandsStack.push(oper.calculate(operand1, operand2));
                } else {
                    operandsStack.push(Double.valueOf(token));
                }
            }
        }

        return operandsStack.pop();
    }

    private static String[] getTokens(String expression) {
        return expression.split(" ");
    }

}
