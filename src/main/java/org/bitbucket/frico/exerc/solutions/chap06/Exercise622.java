package org.bitbucket.frico.exerc.solutions.chap06;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by feliperico on 28/09/15.
 *
 * Problem 6.22: Given a cell phone keypad (specified by a mapping M that
 * takes individual digits and returns the corresponding set of characters)
 * and a number sequence, return all possible character sequences (not just
 * legal words) that correspond to the number sequence.
 *
 */
@SuppressWarnings("unchecked")
public class Exercise622 {

    static final Map<String, List<String>> KEYPAD = new HashMap<String, List<String>>();
    static {
        KEYPAD.put("0", Collections.EMPTY_LIST);
        KEYPAD.put("1", Collections.EMPTY_LIST);
        KEYPAD.put("2", Arrays.asList("A", "B", "C"));
        KEYPAD.put("3", Arrays.asList("D", "E", "F"));
        KEYPAD.put("4", Arrays.asList("G", "H", "I"));
        KEYPAD.put("5", Arrays.asList("J", "K", "L"));
        KEYPAD.put("6", Arrays.asList("M", "N", "O"));
        KEYPAD.put("7", Arrays.asList("P", "Q", "R", "S"));
        KEYPAD.put("8", Arrays.asList("T", "U", "V"));
        KEYPAD.put("9", Arrays.asList("W", "X", "Y", "Z"));
    }

    public static void main(String[] args) {
        printPossibleSequences("726272");
    }

    public static void printPossibleSequences(String numberSeq) {
        AtomicInteger counter = new AtomicInteger(0);
        printRec("", numberSeq, 0, counter);
        System.out.println("Combinations: " + counter.get());
    }

    public static void printRec(String prefix, String numberSeq, int index, AtomicInteger counter) {
        if (index == numberSeq.length()) {
            System.out.println(prefix);
            counter.getAndIncrement();
        } else {
            String number = String.valueOf(numberSeq.charAt(index));
            List<String> seq = KEYPAD.get(number);
            for (String curr : seq) {
                printRec(prefix + curr, numberSeq, index + 1, counter);
            }
        }
    }


}
