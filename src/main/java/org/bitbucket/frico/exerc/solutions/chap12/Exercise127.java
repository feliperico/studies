package org.bitbucket.frico.exerc.solutions.chap12;

import java.util.*;

/**
 * 12.7 Write a function that takes as input a dictionary of English words,
 * and returns a partition of the dictionary into subsets of words that are all
 * anagrams of each other.
 *
 * Created by feliperico on 27/01/16.
 */
public class Exercise127 {



    static void printAnagrams(List<String> dict) {
        Map<String, List<String>> anagrams = new HashMap<>();
        for (String word : dict) {
            String sort = getSort(word);
            if (anagrams.get(sort) == null) {
                anagrams.put(sort, new LinkedList<String>());
            }
            anagrams.get(sort).add(word);
        }

        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            if (entry.getValue().size() >= 2) {
                for (String word : entry.getValue())
                    System.out.print(word + " ");
                System.out.println("");
            }
        }

    }

    static String getSort(String word) {
        char[] charArr = word.toCharArray();
        Arrays.sort(charArr);
        return (new String(charArr)).trim();
    }

}
