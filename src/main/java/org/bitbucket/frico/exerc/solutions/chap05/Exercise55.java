package org.bitbucket.frico.exerc.solutions.chap05;

import java.util.Arrays;
import java.util.List;

/**
 * Created by feliperico on 27/08/15.
 * The power set
 * The power set of a set is the set of all subsets of S, including both
 * the empty set and S itself. The power set of {A,B,C} is
 * {}, {A}, {B}, {C}, {A,B}, {B,C}, {A,C}, {A,B,C}
 *
 * Problem 5.5 IMplement a method that takes as input a set S of distinct
 * elements, and prints the power set of S. Print the subsets one per line,
 * with elements separated by commas.
 *
 */
class Exercise55 {


    public static void main(String[] args) {
        printPowerSet(Arrays.asList(new String[]{"A", "B", "C", "D", "E"}));
    }

    public static void printPowerSet(List<String> set) {
        if (set.size() < 64) {
            for (long i = 0L; i < 1L << set.size(); i++) {
                long current = i;
                while (current != 0) {
                    int index = log2(current & ~(current - 1));
                    System.out.print(set.get(index));
                    current &= (current - 1);
                    if (current != 0) System.out.print(", ");
                }
                System.out.println("");
            }
        }
    }

    public static int log2(long n){
        if(n <= 0) throw new IllegalArgumentException();
        return 63 - Long.numberOfLeadingZeros(n);
    }

}
