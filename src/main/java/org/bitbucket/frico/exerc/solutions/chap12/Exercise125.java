package org.bitbucket.frico.exerc.solutions.chap12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * You are given a sequence of users where each user has a unique 32bit integer key
 * and a set of attributes specified as strings. When you read a user, you should pair
 * that user with another previously read user with identical attributes who is currently
 * unpared, if such a user exists. If the user cannot be paired, you should let him in the
 * unpaired set. How would you implement this matching process efficiently?
 * Created by feliperico on 12/02/16.
 */
public class Exercise125 {

    static class User {
        int id;
        String attr1, attr2, attr3;
        User (int id, String attr1, String attr2, String attr3) {
            this.id = id;
            this.attr1 = attr1;
            this.attr2 = attr2;
            this.attr3 = attr3;
        }

        int hash() {
            int hash = 17;
            if (attr1 != null) hash = hash * 31 + attr1.hashCode();
            if (attr2 != null) hash = hash * 31 + attr2.hashCode();
            if (attr3 != null) hash = hash * 31 + attr3.hashCode();

            return hash;
        }
    }

    static Map<Integer, List<User>> matchUsers(List<User> users) {
        Map<Integer, List<User>> match = new HashMap<>();
        for (int i = 0; i < users.size() - 1; i++) {
            User curr = users.get(i);
            for (int j = i + 1; j < users.size(); j++) {
                User next = users.get(j);
                if (next.hash() == curr.hash()) {
                    if (!match.containsKey(curr.hash())) {
                        match.put(curr.hash(), new ArrayList<User>());
                        match.get(curr.hash()).add(curr);
                    }
                    match.get(curr.hash()).add(next);
                }
            }
        }

        return match;
    }

}
