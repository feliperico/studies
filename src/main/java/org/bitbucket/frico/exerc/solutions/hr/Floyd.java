package org.bitbucket.frico.exerc.solutions.hr;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/floyd-city-of-blinding-lights
 *
 * Created by feliperico on 20/06/16.
 */
@SuppressWarnings("unchecked")
public class Floyd {

/*
Example Input
5 7
1 2 20
1 3 50
1 4 70
1 5 90
2 3 30
3 4 40
4 5 60
8
1 4
5 1
2 5
3 4
1 4
1 2
3 1
1 2

Output

70
-1
130
40
70
20
-1
20
 */
    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int v = scanIn.nextInt(), e = scanIn.nextInt();

        WeightedDigraph g = new WeightedDigraph(v);
        for (int j = 0; j < e; j++) {
            int v1 = scanIn.nextInt(), v2 = scanIn.nextInt(), w = scanIn.nextInt();
            g.addEdge(new DirectedEdge(v1 - 1, v2 - 1, w));
        }

        FloydWarshall sp = new FloydWarshall(g);
        int Q = scanIn.nextInt();
        List<String> output = new ArrayList<>();
        for (int q = 0; q < Q; q++) {
            int v1 = scanIn.nextInt(), v2 = scanIn.nextInt();
            if (sp.distTo[v1 - 1][v2 - 1] < Integer.MAX_VALUE) {
                output.add(String.valueOf(sp.distTo[v1 - 1][v2 - 1]));
            } else {
                output.add("-1");
            }
        }
        scanIn.close();

        for (String out : output) {
            System.out.println(out);
        }
    }

    static class DirectedEdge {
        int to, from, weight;
        DirectedEdge(int from, int to, int weight) {
            this.to = to;
            this.from = from;
            this.weight = weight;
        }
    }

    static class WeightedDigraph {
        int vertices, edges;
        DirectedEdge[][] adjacent;
        WeightedDigraph(int vertices) {
            this.vertices = vertices;
            adjacent = new DirectedEdge[vertices][vertices];
        }
        void addEdge(DirectedEdge edge) {
            adjacent[edge.from][edge.to] = edge;
            edges++;
        }
        Iterator<DirectedEdge> getAdjacent(int i) {
            return new AdjIterator(i);
        }

        class AdjIterator implements Iterator<DirectedEdge> {
            int from, counter;

            AdjIterator(int i) {
                from = i;
            }

            @Override
            public boolean hasNext() {
                while (counter < vertices) {
                    if (adjacent[from][counter] != null) return true;
                    counter++;
                }
                return false;
            }

            @Override
            public DirectedEdge next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return adjacent[from][counter++];
            }
        }

    }

    static class FloydWarshall {

        int[][] distTo;
        DirectedEdge[][] edgeTo;

        FloydWarshall(WeightedDigraph graph) {
            int vertices = graph.vertices;
            distTo = new int[vertices][vertices];
            edgeTo = new DirectedEdge[vertices][vertices];

            // Initialize to maximum
            for (int i = 0; i < vertices; i++) {
                for (int j = 0; j < vertices; j++) {
                    distTo[i][j] = Integer.MAX_VALUE;
                }
            }

            // Initial distances
            for (int v = 0; v < vertices; v++) {
                Iterator<DirectedEdge> iter = graph.getAdjacent(v);
                while (iter.hasNext()) {
                    DirectedEdge edge = iter.next();
                    distTo[edge.from][edge.to] = edge.weight;
                    edgeTo[edge.from][edge.to] = edge;
                }
                // self-loops
                if (distTo[v][v] >= 0) {
                    distTo[v][v] = 0;
                    edgeTo[v][v] = null;
                }
            }

            for (int i = 0; i < vertices; i++) {
                // shortest path using i as intermediary
                for (int v = 0; v < vertices; v++) {
                    if (edgeTo[v][i] == null) continue;
                    for (int w = 0; w < vertices; w++) {
                        if (edgeTo[i][w] == null) continue;
                        if (distTo[v][w] > distTo[v][i] + distTo[i][w]) {
                            distTo[v][w] = distTo[v][i] + distTo[i][w];
                            edgeTo[v][w] = edgeTo[i][w];
                        }
                    }
                }
            }
        }
    }


}
