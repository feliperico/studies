package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Created by feliperico on 26/08/15.
 *
 * The parity bit of a sequence of bits is 1 if the number of 1s
 * in the sequence is odd; otherwise, it is 0. Parity checks are used to detect
 * single bit errors in data storage and communication. It is fairly straightfoward
 * to write code that computes the parity of a single 64-bit non negative integer
 *
 * Problem 5.1: How would you go about computing the parity of a very large
 * number of 64-bit non negative integers?
 */
class Execise51 {

    static void main() {

    }

    /**
     *
     * @param input
     * @return true if parity bit is 1 or false if 0
     */
    static long checkParityBit(long input) {
        long parity = 0;
        while (input != 0) {
            parity ^= (input & 1L);
            input >>= 1;
        }
        return parity;
    }


    /**
     * Eliminating the least significant bit every iteration
     * we can improve the performance of parity bit calculation.
     *
     * @param input
     * @return
     */
    static long checkParityBit2(long input) {
        long parity = 0;
        while (input != 0) {
            parity ^= 1L;
            input &= (input - 1);
        }
        return parity;
    }

    /***
     * For large numbers of parity checks, we can improve performance by using
     * a cache that stores the parity bit for 16bits numbers.. The point is that we don't
     * need to calculate for every 64bit non negative integers, which helps us saving
     * memory. The precomputedParity can be calculated on demand.
     *
     * @param input
     * @param precomputedParity
     * @return
     */
//    static short checkParityBit3(long input, short[] precomputedParity) {
//        return precomputedParity[input>>48 & 0xFFFF] ^
//                precomputedParity[input>>32 & 0xFFFF] ^
//                precomputedParity[input>>16 & 0xFFFF] ^
//                precomputedParity[input & 0xFFFF]
//    }
}
