package org.bitbucket.frico.exerc.solutions.chap13;

/**
 * You are a photographer for a soccer meet. You will be taking pictures of pairs of
 * opposing teams. All teams have the same number of players. A team photo consists
 * of a front row of players and a back row of players. A player in the back row must
 * be taller than the player in front of hom, as illustrated in Figure 13.1. All players in a
 * row must be from the same team.
 * Design an algorithm that takes as input two teams and the heights of the players
 * in the teams and checks if it is possible to place players to take the photo
 * subject to the placement constraint.
 *
 * Created by feliperico on 04/04/16.
 */
public class Exercise136 {


    static boolean isThePhotoPossible(int[] a, int[] b) {

        int[] minor = a[0] < b[0] ? a : b;
        int[] greater = a[0] < b[0] ? b : a;

        // if the arrays are not sorted, they should be here

        for (int i = 0; i < 10; i++) {
            if (!(minor[i] < greater[i])) {
                return false;
            }
        }

        return true;
    }

}
