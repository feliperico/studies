package org.bitbucket.frico.exerc.solutions.chap16;

import org.bitbucket.frico.ds.Queue;
import org.bitbucket.frico.ds.impl.LinkedListQueue;

import java.util.List;

/**
 *
 * 16.3 Consider a collection of electrical pins on a printed circuit board (PCB). For each
 * pair of pins, there may or may not be a wire joining them. This is shown on figure 16.5,
 * where vertices correspond to pins, and edges indicate the presence of a wire between pins.
 * Design an algorithm that takes a set of pins and a set of wires connecting pairs of pins, and
 * determine if it is possible to place some pins on the left half of a PCB, and the remainder on
 * the right half, such that each wire is between left and right halves. Return such a division if
 * one exists.
 *
 * Created by feliperico on 07/03/16.
 */
public class Exercise163 {

    static class Vertex {
        int num, d = -1;
        List<Vertex> adj;

    }


    static boolean checkBipartiteViaBFS(Vertex v) {
        Queue<Vertex> q = new LinkedListQueue<>();
        q.enqueue(v);
        while (!q.isEmpty()) {
            Vertex curr = q.dequeue();
            for (Vertex w : curr.adj) {
                if (w.d == -1) {
                    w.d = curr.d + 1;
                    q.enqueue(w);
                } else if (w.d == curr.d) {
                    return false;
                }
            }
        }

        return true;
    }


    static boolean isBipartite(List<Vertex> graph) {
        for (Vertex v : graph) {
            if (v.d == -1) {
                v.d = 0;
                if (!checkBipartiteViaBFS(v)) {
                    return false;
                }
            }
        }
        return true;
    }


}
