package org.bitbucket.frico.exerc.solutions.chap12;

import org.bitbucket.frico.ds.SymbolTable;
import org.bitbucket.frico.ds.impl.HashChainSymbolTable;

/**
 * A hash table can be viewed as a dictionary. For this reason,
 * hash tables commonly appear in string processing.
 * You are required to write a method which takes an anonymous letter
 * L and text from a magazine M. Your method is to return true iff L can
 * be written using M, i.e., if a letter appear k times in L, it must
 * appear at least k times in M as well.
 * Created by feliperico on 13/01/16.
 */
public class Exercise129 {


    public static void main(String[] args) {
        String L = "aaaabbbcccccccccccddddddeef";
        String M = "aaaabbbbbbbbbbbbbbbbcccccccccccddddddeefffffffffff";

        System.out.println(canItBeWritten(L, M));

        M = "aaaabbbbbbbbbbbbbbbbccccccccccc9999999999999999999ddddddefffffffffff";

        System.out.println(canItBeWritten(L, M));
    }


    static boolean canItBeWritten(String L, String M) {
        SymbolTable<Character, Integer> charCounterST = new HashChainSymbolTable<>();
        for (int i = 0; i < L.length(); i++) {
            char curr = L.charAt(i);
            int currCount = charCounterST.get(curr) != null ? charCounterST.get(curr) + 1 : 1;
            charCounterST.put(curr, currCount);
        }

        int counter = L.length();
        for (int i = 0; i < M.length(); i++) {
            char curr = M.charAt(i);
            if (charCounterST.get(curr) != null && charCounterST.get(curr) > 0) {
                charCounterST.put(curr, charCounterST.get(curr) - 1);
                counter--;
            }
            if (counter == 0 ) {
                return true;
            }
        }
        return false;
    }
}
