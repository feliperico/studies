package org.bitbucket.frico.exerc.solutions.hr;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/bfsshortreach
 *
 * Created by feliperico on 02/06/16.
 */
@SuppressWarnings("unchecked")
public class BfsShortReach {
    private static final int EDGE_DIST_SIZE = 6;

    private static class Graph {
        List<Integer>[] vertices;
        int verticesNum;
        int edgesNum;

        Graph(int v) {
            verticesNum = v;
            vertices = (List<Integer>[]) new List[verticesNum];
            for (int i = 0; i < verticesNum; i++) {
                vertices[i] = new ArrayList<>();
            }
        }

        public void addEdge(int v, int w) {
            vertices[v].add(w);
            vertices[w].add(v);
            edgesNum++;
        }

        public Iterable<Integer> adjacent(int v) {
            if (v >= 0 && v < verticesNum) {
                return vertices[v];
            }
            return Collections.emptyList();
        }
    }

    private static class BFS {

        boolean[] visited;
        int[] edgeTo;
        int source;

        BFS(Graph g, int s) {
            visited = new boolean[g.verticesNum];
            edgeTo = new int[g.verticesNum];
            source = s;
            bfs(g, s);
        }

        private void bfs(Graph g, int s) {
            visited[s] = true;
            Queue<Integer> q = new LinkedList<>();
            q.add(s);
            while (!q.isEmpty()) {
                int v = q.remove();
                for (int w : g.adjacent(v)) {
                    if (!visited[w]) {
                        q.add(w);
                        visited[w] = true;
                        edgeTo[w] = v;
                    }
                }
            }
        }

        boolean isConnected(int w) {
            return visited[w];
        }

        int getDistance(int w) {
            if (visited[w]) {
                int dist = 1;
                int curr = edgeTo[w];
                while(curr != source) {
                    dist++;
                    curr = edgeTo[curr];
                }
                return dist;
            }
            return 0;
        }
    }

    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int testCases = scanIn.nextInt();
        String[] outputs = new String[testCases];

        for (int i = 0; i < testCases; i++) {
            int v = scanIn.nextInt(), e = scanIn.nextInt();
            Graph g = new Graph(v);
            for (int j = 0; j < e; j++) {
                int v1 = scanIn.nextInt(), v2 = scanIn.nextInt();
                g.addEdge(v1 - 1, v2 - 1);
            }
            int s = scanIn.nextInt();
            outputs[i] = getDistancesResult(s, g);
        }
        scanIn.close();

        for (int i = 0; i < outputs.length; i++) {
            System.out.println(outputs[i]);
        }
    }

    static String getDistancesResult(int s, Graph g) {
        BFS bfs = new BFS(g, s - 1);
        StringBuilder sbDist = new StringBuilder();
        for (int i = 0; i < g.verticesNum; i++) {
            if (i != s - 1) {
                if (bfs.isConnected(i)) {
                    sbDist.append(bfs.getDistance(i) * EDGE_DIST_SIZE).append(" ");
                } else {
                    sbDist.append("-1").append(" ");
                }
            }
        }
        return sbDist.toString().trim();
    }

}
