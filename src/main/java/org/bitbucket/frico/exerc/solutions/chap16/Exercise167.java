package org.bitbucket.frico.exerc.solutions.chap16;

import org.bitbucket.frico.ds.Stack;
import org.bitbucket.frico.ds.impl.LinkedListStack;

import java.util.List;

/**
 * How would you generalize your solution to Problem 13.6 on Page 100
 * to determine the largest number of teams that can be photographed simultaneously
 * subject to the same constraints?
 *
 * Created by feliperico on 06/04/16.
 */
public class Exercise167 {

    static class TeamVertex {
        List<TeamVertex> adjacent;
        int maxDistance = 1;
        boolean visited;
    }


    static Stack<TeamVertex> getTopologicalOrderStack(List<TeamVertex> graph) {
        Stack<TeamVertex> ordered = new LinkedListStack<>();
        for (TeamVertex tv : graph) {
            if (!tv.visited) {
                dfs(tv, ordered);
            }
        }
        return ordered;
    }

    static void dfs(TeamVertex vertex, Stack<TeamVertex> stack) {
        vertex.visited = true;
        for (TeamVertex tv : vertex.adjacent) {
            if (!tv.visited) {
                dfs(tv, stack);
            }
        }
        stack.push(vertex);
    }

    static int getMaxDistance(Stack<TeamVertex> ordered) {
        int maxDist = 0;
        while (!ordered.isEmpty()) {
            TeamVertex top = ordered.pop();
            maxDist = Math.max(top.maxDistance, maxDist);
            for (TeamVertex tv : top.adjacent) {
                tv.maxDistance = Math.max(tv.maxDistance, top.maxDistance + 1);
            }
        }

        return maxDist;
    }

    static int findMaximumNumberOfTeamsInAPhoto(List<TeamVertex> graph) {
        Stack<TeamVertex> topologicalOrdering = getTopologicalOrderStack(graph);
        return getMaxDistance(topologicalOrdering);
    }


}
