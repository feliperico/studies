package org.bitbucket.frico.exerc.solutions.hr;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/dijkstrashortreach
 *
 * Created by feliperico on 07/06/16.
 */
@SuppressWarnings("unchecked")
public class DijkstraShortReach {

    static class Edge {
        int v, w, weight;

        Edge(int v, int w, int weight) {
            this.v = v;
            this.w = w;
            this.weight = weight;
        }

        public int either() {
            return v;
        }

        public int other(int x) {
            if (x == v) return w;
            return v;
        }
    }

    static class WeightedGraph {
        int vertices;
        int edges;
        List<Edge>[] adjacent;

        WeightedGraph(int vertices) {
            this.vertices = vertices;
            adjacent = (List<Edge>[]) new List[vertices];
            for (int i = 0; i < this.vertices; i++) {
                adjacent[i] = new LinkedList<>();
            }
        }

        Iterable<Edge> getAdjacent(int v) {
            return adjacent[v];
        }

        void addEdge(Edge edge) {
            int v = edge.either(), w = edge.other(v);
            adjacent[v].add(edge);
            adjacent[w].add(edge);
            edges++;
        }
    }

    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int testCases = scanIn.nextInt();
        String[] outputs = new String[testCases];

        for (int i = 0; i < testCases; i++) {
            int v = scanIn.nextInt(), e = scanIn.nextInt();
            WeightedGraph g = new WeightedGraph(v);
            for (int j = 0; j < e; j++) {
                int v1 = scanIn.nextInt(), v2 = scanIn.nextInt(), w = scanIn.nextInt();
                g.addEdge(new Edge(v1 - 1, v2 - 1, w));
            }
            int s = scanIn.nextInt();
            outputs[i] = getShortReachResult(s, g);
        }
        scanIn.close();

        for (int i = 0; i < outputs.length; i++) {
            System.out.println(outputs[i]);
        }
    }

    static String getShortReachResult(int source, WeightedGraph graph) {
        DijkstraSP sp = new DijkstraSP(graph, source - 1);
        StringBuilder sbResult = new StringBuilder();
        for (int i = 0; i < graph.vertices; i++) {
            if (i != source - 1) {
                if (sp.hasPathTo(i)) {
                    sbResult.append(sp.distTo(i)).append(" ");
                } else {
                    sbResult.append("-1 ");
                }
            }
        }
        return sbResult.toString().trim();
    }

    static class DijkstraSP {

        int source;
        int[] edgeTo;
        int[] distTo;
        IndexMinPQ pq;

        DijkstraSP(WeightedGraph graph, int source) {
            this.source = source;
            edgeTo = new int[graph.vertices];
            distTo = new int[graph.vertices];
            pq = new IndexMinPQ(graph.vertices);
            for (int i = 0; i < graph.vertices; i++) {
                distTo[i] = Integer.MAX_VALUE; // infinity
            }
            distTo[source] = 0;

            pq.insert(source, 0);
            while (!pq.isEmpty()) {
                relaxAdj(graph, pq.delMin());
            }

        }

        private void relaxAdj(WeightedGraph graph, int v) {
            for (Edge e : graph.getAdjacent(v)) {
                relax(e, v);
            }
        }
        private void relax(Edge e, int v) {
            int w = e.other(v);
            if (distTo[w] > distTo[v] + e.weight) {
                distTo[w] = distTo[v] + e.weight;
                edgeTo[w] = v;
                if (pq.contains(w)) {
                    pq.change(w, distTo(w));
                } else {
                    pq.insert(w, distTo(w));
                }
            }
        }

        public boolean hasPathTo(int v) {
            return distTo[v] < Integer.MAX_VALUE;
        }

        public int distTo(int v) {
            return distTo[v];
        }
    }

    static class IndexMinPQ {
        int[] array;
        int[] priorityQueue;
        int[] indexToPQ;
        int size;

        IndexMinPQ(int capacity) {
            array = new int[capacity + 1];
            priorityQueue = new int[capacity + 1];
            indexToPQ = new int[capacity + 1];
            for (int i = 0; i < capacity + 1; i++) {
                indexToPQ[i] = -1;
            }
        }

        private void swap(int i, int j) {
            int temp = priorityQueue[i];
            priorityQueue[i] = priorityQueue[j];
            priorityQueue[j] = temp;
            indexToPQ[priorityQueue[i]] = i;
            indexToPQ[priorityQueue[j]] = j;
        }

        private boolean greater(int i, int j) {
            return array[priorityQueue[i]] > array[priorityQueue[j]];
        }

        private void swim(int i) {
            while (i > 1 && greater(i/2, i)) {
                swap(i, i/2);
                i = i/2;
            }
        }

        private void sink(int i) {
            while (2 * i <= size) {
                int j = i * 2;
                if (j < size && greater(j, j + 1)) j++;
                if (!greater(i, j)) break;
                swap(i, j);
                i = j;
            }
        }

        public int delMin() {
            int min = priorityQueue[1];
            swap(1, size--);
            sink(1);
            //assert min == priorityQueue[size+1];
            indexToPQ[min] = -1;
            array[min] = -1; // to help with garbage collection
            priorityQueue[size+1] = -1; // not needed
            return min;
        }

        public void insert(int index, int weight) {
            size++;
            indexToPQ[index] = size;
            priorityQueue[size] = index;
            array[index] = weight;
            swim(size);
        }

        public boolean contains(int index) {
            return indexToPQ[index] != -1;
        }

        public void change(int index, int weight) {
            array[index] = weight;
            swim(indexToPQ[index]);
            sink(indexToPQ[index]);
        }

        public boolean isEmpty() {
            return size <= 0;
        }

    }
}
