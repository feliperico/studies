package org.bitbucket.frico.exerc.solutions.chap16;

import org.bitbucket.frico.ds.Graph;
import org.bitbucket.frico.algs.ConnectedComponents;

import java.util.List;

/**
 * Given a set of variables x1,x2,...xn, equality constraints of the form xi = xj
 * and inequality constraints of the form xi != xj. Is it possible to satisfy all
 * the constraints simultaneously? For example, the constraints x1 = x2, x2 = x3, x3 = x4
 * and x1 != x4 cannot be satisfied simultaneously.
 * Design an efficient algorithm that takes as input a collection of equality and inequality
 * constraints and decides whether the constraints can be satisfied simultaneously.
 *
 * Created by feliperico on 07/03/16.
 */
public class Exercise166 {


    static class Constraint {
        int i, j;
    }

    static boolean canBeSatisfied(List<Constraint> equalities, List<Constraint> inequalities, int numberOfVars) {

        Graph g = new Graph(numberOfVars);
        for (Constraint c : equalities) {
            g.addEdge(c.i, c.j);
        }

        ConnectedComponents cc = new ConnectedComponents(g);

        for (Constraint c : inequalities) {
            if (cc.isConnected(c.i, c.j)) {
                return false;
            }
        }

        return true;
    }
}
