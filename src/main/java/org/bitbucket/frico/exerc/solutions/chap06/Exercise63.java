package org.bitbucket.frico.exerc.solutions.chap06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by feliperico on 30/09/15.
 *
 * 6.3 Max Difference
 * The problem of computing the maximum difference in an array, specifically
 * maxi>j(A[i] - A[j]) arises in a number of contexts. We introduce this problem
 * in the context of historical stock quote information on Page 1. Here we study
 * another application of the same problem.
 * A robot needs to travel along a path that includes several ascents and descents.
 * When it goes up, it uses its battery to power the motor and when it descends,
 * it recovers the energy which is stored in the battery. The battery recharging process
 * is ideal: on descending, every Joule of gravitational potential energy converts
 * to a Joule of electrical energy that is stored in the battery. The battery has a
 * limited capacity and once it reaches this capacity, the energy generated in
 * descending is lost.
 * Problem 6.3: Design an algorithm that takes a sequence of n three dimensional
 * coordinates to be traversed, and returns the minimum battery capacity needed to
 * complete the journey. The robot begins with a fully charged battery.
 */
public class Exercise63 {

    public static void main(String[] args) {
        Integer[] zCoords = randomArr(20);
        System.out.println(getMinimumBatteryCapacity(zCoords));
        System.out.println(getMinimumBatteryCapacity2(zCoords));
        System.out.println(getMinimumBatteryCapacity(zCoords));
        System.out.println(getMinimumBatteryCapacity2(zCoords));
    }

    public static Integer[] randomArr(int length) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < length; i++) {
            list.add(ThreadLocalRandom.current().nextInt(-100, 101));
        }
        Collections.shuffle(list);
        return list.toArray(new Integer[0]);
    }
    public static int getMinimumBatteryCapacity(Integer[] zCoordinates) {

        int minCoordIndex = 0, maxCoordIndex = 0, possibleMin = 0;

        for (int i = 1; i < zCoordinates.length; i++) {
            if (zCoordinates[i] > zCoordinates[maxCoordIndex]) {
                maxCoordIndex = i;
                if (possibleMin != minCoordIndex) {
                    minCoordIndex = possibleMin;
                }
            } else if (zCoordinates[i] < zCoordinates[possibleMin]) {
                possibleMin = i;
            }
        }

        if (possibleMin < maxCoordIndex &&
                zCoordinates[possibleMin] < zCoordinates[minCoordIndex]) {
            minCoordIndex = possibleMin;
        }

        int minCapacity = zCoordinates[maxCoordIndex] - zCoordinates[minCoordIndex];

        return minCapacity > 0 ? minCapacity : 0;
    }

    public static int getMinimumBatteryCapacity2(Integer[] zCoordinates) {
        int capacity = 0, minCoordIndex = 0;

        for (int i = 0; i < zCoordinates.length; i++) {
            capacity = Math.max(capacity, zCoordinates[i] - zCoordinates[minCoordIndex]);
            if (zCoordinates[i] < zCoordinates[minCoordIndex])  minCoordIndex = i;
        }

        return capacity;
    }
}
