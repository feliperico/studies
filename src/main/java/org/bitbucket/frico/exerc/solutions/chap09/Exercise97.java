package org.bitbucket.frico.exerc.solutions.chap09;

/**
 * Given an inorder traversal order and one of preorder or postorder traversal order
 * of a binary tree, write a function to reconstruct the tree.
 *
 * Created by feliperico on 04/01/16.
 */
public class Exercise97 {

    private static class Node {
        Node left;
        Node right;
        Node parent;
        int data;
        Node(int data, Node parent) {
            this.data = data;
            this.parent = parent;
        }
    }

    public static void main(String[] args) {
        int[] preorder = new int[] {1,2,4,8,5,9,10,3,6,7,11,13,14,12,15,16,17,18,19,20};
        int[] inorder = new int[] {8,4,2,9,5,10,1,6,3,13,11,14,7,15,12,17,16,18,19,20};

        Node reconstructed = reconstructTreeBookSolution(inorder, preorder);

        try {
            checkEqualTrees(reconstructed, getOriginal());
            System.out.println("Works!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static void checkEqualTrees(Node tree1, Node tree2) {
        if (tree1 != null && tree2 != null) {
            checkEqualTrees(tree1.left, tree2.left);
            if (tree1.data != tree2.data) throw new RuntimeException("Not equal!");
            checkEqualTrees(tree1.right, tree2.right);
        } else if ((tree1 == null && tree2 != null) ||
                (tree1 != null && tree2 == null)) {
            throw new RuntimeException("Not equal!");
        }
    }
    static Node getRoot(int[] inorder, int inStart, int inEnd, int[] preorder, int preStart, int preEnd, Node parent) {
        if (inStart <= inEnd && preStart <= preEnd) {
            int preRootIdx = -1;
            for (int i = inStart; i <= inEnd; i++) {
                if (inorder[i] == preorder[preStart]) {
                    preRootIdx = i;
                    break;
                }
            }
            if (preRootIdx != -1) {
                Node root = new Node(preorder[preStart], parent);
                int leftSubtreeSize = preRootIdx - inStart;
                root.left = getRoot(inorder, inStart, preRootIdx - 1, preorder, preStart + 1, preStart + leftSubtreeSize, root);
                root.right = getRoot(inorder, preRootIdx + 1, inEnd, preorder, preStart + 1 + leftSubtreeSize, preEnd, root);
                return root;
            }
        }
        return null;
    }

    static Node reconstructTreeBookSolution(int[] inorder, int[] preorder) {
        return getRoot(inorder, 0, inorder.length - 1, preorder, 0, preorder.length - 1, null);
    }

    static Node reconstructTree(int[] inorder, int[] preorder) {
        Node curr, root = new Node(preorder[0], null);
        int nextPreorder = 1, rootIdx = getIndex(root.data, inorder);
        curr = root;

        while (nextPreorder < preorder.length) {
            int currIdx = getIndex(curr.data, inorder);
            int nextIdx = getIndex(preorder[nextPreorder], inorder);
            int parentIdx = curr.parent != null ? getIndex(curr.parent.data, inorder) : -1;

            if (currIdx < rootIdx && nextIdx > rootIdx) {
                curr = root;
                currIdx = rootIdx;
                parentIdx = -1;
            }

            if (parentIdx == -1
                    || (isLeftChild(curr) && nextIdx < parentIdx)
                    || (isRightChild(curr) && nextIdx > parentIdx)) {
                Node next = new Node(preorder[nextPreorder++], curr);
                if (nextIdx < currIdx) {
                    curr.left = next;
                } else if (nextIdx > currIdx) {
                    curr.right = next;
                }
                curr = next;
            } else {
                curr = curr.parent;
            }

        }

        return root;
    }

    static boolean isLeftChild(Node node) {
        if (node.parent != null) {
            Node parent = node.parent;
            return parent.left == node;
        }
        return false;
    }

    static boolean isRightChild(Node node) {
        if (node.parent != null) {
            Node parent = node.parent;
            return parent.right == node;
        }
        return false;
    }

    static int getIndex(int num, int[] array) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (num == array[i]) {
                index = i;
                break;
            }
        }
        return index;
    }

    static Node getOriginal() {
        Node root = new Node(1, null);
        Node node2 = new Node(2, root);
        root.left = node2;
        Node node3 = new Node(3, root);
        root.right = node3;
        Node node4 = new Node(4, node2);
        node2.left = node4;
        Node node5 = new Node(5, node2);
        node2.right = node5;
        Node node8 = new Node(8, node4);
        node4.left = node8;
        Node node9 = new Node(9, node5);
        node5.left = node9;
        Node node10 = new Node(10, node5);
        node5.right = node10;
        Node node6 = new Node(6, node3);
        node3.left = node6;
        Node node7 = new Node(7, node3);
        node3.right = node7;
        Node node11 = new Node(11, node7);
        node7.left = node11;
        Node node12 = new Node(12, node7);
        node7.right = node12;
        Node node13 = new Node(13, node11);
        node11.left = node13;
        Node node14 = new Node(14, node11);
        node11.right = node14;
        Node node15 = new Node(15, node12);
        node12.left = node15;
        Node node16 = new Node(16, node12);
        node12.right = node16;
        Node node17 = new Node(17, node16);
        Node node18 = new Node(18, node16);
        node16.left = node17;
        node16.right = node18;
        Node node19 = new Node(19, node18);
        node18.right = node19;
        node19.right = new Node(20, node19);
        return root;
    }
}
