package org.bitbucket.frico.exerc.solutions.chap09;

import org.bitbucket.frico.ds.Stack;
import org.bitbucket.frico.ds.impl.LinkedListStack;

/**
 * Design an O(n) algorithm for reconstructing a binary tree
 * from a preorder visit sequence that uses null to mark empty children.
 *
 * Created by feliperico on 10/01/16.
 */
public class Exercise98 {


    private static class Node {
        Node right, left, parent;
        String data;
        boolean leftFound, rightFound;
        Node(String data, Node parent) {
            this.data = data;
            this.parent = parent;
        }
        boolean isComplete() {
            return leftFound && rightFound;
        }
    }

    public static void main(String[] args) {
        String[] preorderArray = new String[] { "H", "B", "F", null, null, "E", "A", null, null, null, "C", null, "D", null, "G", "I", null, null, null};

        Node root = reconstructFromPreorder(preorderArray);
        Node original = getOriginal();

        try {
            checkEqualTrees(root, original);
            System.out.println("Works!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        root = reconstructingFromPreorderBookSolution(preorderArray);

        try {
            checkEqualTrees(root, original);
            System.out.println("Works!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static void checkEqualTrees(Node tree1, Node tree2) {
        if (tree1 != null && tree2 != null) {
            checkEqualTrees(tree1.left, tree2.left);
            if (!tree1.data.equals(tree2.data)) throw new RuntimeException("Not equal!");
            checkEqualTrees(tree1.right, tree2.right);
        } else if ((tree1 == null && tree2 != null) ||
                (tree1 != null && tree2 == null)) {
            throw new RuntimeException("Not equal!");
        }
    }

    static Node getOriginal() {
        Node root = new Node("H", null);
        Node nodeB = new Node("B",  root);
        root.left = nodeB;
        Node nodeC = new Node("C", root);
        root.right = nodeC;
        Node nodeF = new Node("F",nodeB);
        nodeB.left = nodeF;
        Node nodeE = new Node("E", nodeB);
        nodeB.right = nodeE;
        Node nodeA = new Node("A", nodeE);
        nodeE.left = nodeA;
        Node nodeD = new Node("D", nodeC);
        nodeC.right = nodeD;
        Node nodeG = new Node("G", nodeD);
        nodeD.right = nodeG;
        Node nodeI = new Node("I", nodeG);
        nodeG.left = nodeI;
        return root;
    }

    static Node reconstructFromPreorder(String[] preorderArray) {
        Node root = new Node(preorderArray[0], null), parent = root;

        for (int i = 1; i < preorderArray.length; i++) {
            while (parent.isComplete()) parent = parent.parent;

            Node curr = null;
            if (preorderArray[i] != null) curr = new Node(preorderArray[i], parent);

            if (!parent.leftFound) {
                parent.left = curr; parent.leftFound = true;
            } else {
                parent.right = curr; parent.rightFound = true;
            }

            if (curr != null) parent = curr;
        }

        return root;
    }

    static Node reconstructingFromPreorderBookSolution(String[] preorderArray) {
        Stack<Node> stack = new LinkedListStack<Node>();

        for (int i = preorderArray.length - 1; i >= 0; i--) {
            if (preorderArray[i] == null) {
                stack.push(null);
            } else {
                Node curr = new Node(preorderArray[i], null);
                Node left = stack.pop();
                Node right = stack.pop();
                curr.left = left;
                if (left != null) left.parent = curr;
                curr.right =  right;
                if (right != null) right.parent = curr;
                stack.push(curr);
            }
        }

        return stack.pop();
    }

}
