package org.bitbucket.frico.exerc.solutions.chap14;

import java.util.ArrayList;
import java.util.List;

/**
 * A BST is a sorted structure which suggests that it should be possible
 * to find the k largest keys easily.
 * Given the root of a BST and an integer k, design a function that find the k
 * largest elements in this BST.
 *
 * Created by feliperico on 18/02/16.
 */
public class Exercise1411 {

    static class Node {
        Node left, right;
        int key;
        Node(int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        Node root = getTree();
        List<Integer> kLargestKeys = getKLargestKeys(root, 3);
        for (Integer key : kLargestKeys) {
            System.out.println(key);
        }
    }

    static Node getTree() {
        return new Node(19, new Node(7, new Node(3, new Node(2,null,null), new Node(5, null, null)), new Node(11, null, new Node(17, new Node(13, null, null), null))), new Node(43, new Node(23, null, new Node(37, new Node(29, null, new Node(31, null, null)), new Node(41, null, null))), new Node(47, null, new Node(53, null, null))));
    }

    static List<Integer> getKLargestKeys(Node root, int k) {
        List<Integer> kLargestKeys = new ArrayList<>();
        getKLargestKeys(root, k, kLargestKeys);
        return kLargestKeys;
    }

    static void getKLargestKeys(Node root, int k, List<Integer> kLargestKeys) {
        if (root != null) {
            getKLargestKeys(root.right, k, kLargestKeys);
            if (kLargestKeys.size() < k)
                kLargestKeys.add(root.key);
            if (kLargestKeys.size() < k)
                getKLargestKeys(root.left, k, kLargestKeys);
        }
    }
}
