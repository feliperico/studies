package org.bitbucket.frico.exerc.solutions.chap09;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two nodes in a binary tree T, design an algorithm that computes
 * their LCA. Assume that each node has a parent pointer. The tree has n nodes
 * and height h. Your algorithm should run in O(1) space and O(h) time.
 *
 * Created by feliperico on 05/01/16.
 */
public class Exercise912 {
    private static class Node {
        Node left, right, parent;
        int data;
        Node(int data, Node parent) {
            this.data = data;
            this.parent = parent;
        }
    }

    public static void main(String[] args) {
        Node root = new Node(1, null);
        Node node2 = new Node(2, root);
        root.left = node2;
        Node node3 = new Node(3, root);
        root.right = node3;
        Node node4 = new Node(4, node2);
        node2.left = node4;
        Node node5 = new Node(5, node2);
        node2.right = node5;
        Node node8 = new Node(8, node4);
        node4.left = node8;
        Node node9 = new Node(9, node5);
        node5.left = node9;
        Node node10 = new Node(10, node5);
        node5.right = node10;
        Node node6 = new Node(6, node3);
        node3.left = node6;
        Node node7 = new Node(7, node3);
        node3.right = node7;
        Node node11 = new Node(11, node7);
        node7.left = node11;
        Node node12 = new Node(12, node7);
        node7.right = node12;
        Node node13 = new Node(13, node11);
        node11.left = node13;
        Node node14 = new Node(14, node11);
        node11.right = node14;
        Node node15 = new Node(15, node12);
        node12.left = node15;
        Node node16 = new Node(16, node12);
        node12.right = node16;
        Node node17 = new Node(17, node16);
        Node node18 = new Node(18, node16);
        node16.left = node17;
        node16.right = node18;
        Node node19 = new Node(19, node18);
        node18.right = node19;
        node19.right = new Node(20, node19);

        System.out.println(getLowestCommonAncestor(node2, node3).data);
        System.out.println(getLowestCommonAncestor(node8, node9).data);
        System.out.println(getLowestCommonAncestor(node10, node9).data);
        System.out.println(getLowestCommonAncestor(node13, node19).data);
        System.out.println(getLowestCommonAncestor(node9, node2).data);
        System.out.println("------------------------");
        System.out.println(getLowestCommonAncestorAlternative(node2, node3).data);
        System.out.println(getLowestCommonAncestorAlternative(node8, node9).data);
        System.out.println(getLowestCommonAncestorAlternative(node10, node9).data);
        System.out.println(getLowestCommonAncestorAlternative(node13, node19).data);
        System.out.println(getLowestCommonAncestorAlternative(node9, node2).data);
    }

    static Node getLowestCommonAncestor(Node n1, Node n2) {
        Node currN1 = n1, currN2 = n2;
        int n1Size = 0, n2Size = 0;

        while (currN1 != null || currN2 != null) {
            if (currN1 != null) {
                n1Size++;
                currN1 = currN1.parent;
            }
            if (currN2 != null) {
                n2Size++;
                currN2 = currN2.parent;
            }
        }

        currN2 = n2;
        currN1 = n1;
        if (n1Size > n2Size) {
            for (int i = 0; i < n1Size - n2Size; i++) {
                currN1 = currN1.parent;
            }
        } else if (n2Size > n1Size) {
            for (int i = 0; i < n2Size - n1Size; i++) {
                currN2 = currN2.parent;
            }
        }

        while (currN1.data != currN2.data) {
            currN1 = currN1.parent;
            currN2 = currN2.parent;
        }

        return currN1;
    }

    static Node getLowestCommonAncestorAlternative(Node n1, Node n2) {
        Node curr1 = n1, curr2 = n2;
        Map<Integer, Node> map = new HashMap<Integer, Node>();
        while (curr1 != null || curr2 != null) {
            if (curr1 != null) {
                if (map.containsKey(curr1.data)) {
                    return map.get(curr1.data);
                }
                map.put(curr1.data, curr1);
                curr1 = curr1.parent;
            }
            if (curr2 != null) {
                if (map.containsKey(curr2.data)) {
                    return map.get(curr2.data);
                }
                map.put(curr2.data, curr2);
                curr2 = curr2.parent;
            }
        }
        return null;
    }
}
