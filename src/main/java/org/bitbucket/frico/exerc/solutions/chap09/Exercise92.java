package org.bitbucket.frico.exerc.solutions.chap09;

/**
 * Define a node in a binary tree to be k-balanced if the difference between
 * the number of nodes in its left and right subtrees is no more than k.
 * Design an algorithm that takes as input a binary tree and a positive integer k,
 * and returns a node u in the binary tree such that u is not k-balanced, but all
 * of u's descendants are k-balanced. If no such node exists return null.
 * Created by feliperico on 05/01/16.
 */
public class Exercise92 {
    private static class Node {
        Node left, right, parent;
        int data, count = -1;
        Node(int data, Node parent) {
            this.data = data;
            this.parent = parent;
        }
    }

    public static void main(String[] args) {
        Node root = new Node(1, null);
        Node node2 = new Node(2, root);
        root.left = node2;
        Node node3 = new Node(3, root);
        root.right = node3;
        Node node4 = new Node(4, node2);
        node2.left = node4;
        Node node5 = new Node(5, node2);
        node2.right = node5;
        Node node8 = new Node(8, node4);
        node4.left = node8;
        Node node9 = new Node(9, node5);
        node5.left = node9;
        Node node10 = new Node(10, node5);
        node5.right = node10;
        Node node6 = new Node(6, node3);
        node3.left = node6;
        Node node7 = new Node(7, node3);
        node3.right = node7;
        Node node11 = new Node(11, node7);
        node7.left = node11;
        Node node12 = new Node(12, node7);
        node7.right = node12;
        Node node13 = new Node(13, node11);
        node11.left = node13;
        Node node14 = new Node(14, node11);
        node11.right = node14;
        Node node15 = new Node(15, node12);
        node12.left = node15;
        Node node16 = new Node(16, node12);
        node12.right = node16;
        Node node17 = new Node(17, node16);
        Node node18 = new Node(18, node16);
        node16.left = node17;
        node16.right = node18;
        Node node19 = new Node(19, node18);
        node18.right = node19;
        node19.right = new Node(20, node19);

        Node result = getNotKBalanced(root, 3);
        System.out.println(result != null ? result.data : "Not found!");
    }

    static Node getNotKBalanced(Node root, int k) {
        if (root != null) {
            if (!isKBalanced(root, k) && isKBalanced(root.left, k) && isKBalanced(root.right, k)) {
                return root;
            } else {
                Node ret = getNotKBalanced(root.left, k);
                if (ret == null) {
                    ret = getNotKBalanced(root.right, k);
                }
                return ret;
            }
        }
        return null;
    }

    static boolean isKBalanced(Node node, int k) {
        countNodes(node);
        int countLeft = countNodes(node.left), countRight = countNodes(node.right);
        return Math.abs(countLeft - countRight) <= k;
    }

    static int countNodes(Node node) {
        if (node != null) {
            if (node.count == -1) {
                node.count = 1 + countNodes(node.left) + countNodes(node.right);
            }
            return node.count;
        }
        return 0;
    }

}
