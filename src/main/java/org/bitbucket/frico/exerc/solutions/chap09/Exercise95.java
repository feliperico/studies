package org.bitbucket.frico.exerc.solutions.chap09;

import java.util.ArrayList;
import java.util.List;

/**
 * Let T be the root of a binary tree in which nodes have an explicit parent field.
 * Design an iterative algorithm that enumerates the nodes inorder and uses O(1)
 * additional space. Your algorithm cannot modify the tree.
 *
 * Created by feliperico on 02/01/16.
 */
public class Exercise95 {

    public static class Node {
        Node parent, right, left;
        int data;
        public Node(int data, Node parent) {
            this.parent = parent;
            this.data = data;
        }
    }

    public static void main(String[] args) {
        Node root = new Node(1, null);
        Node node2 = new Node(2, root);
        root.left = node2;
        Node node3 = new Node(3, root);
        root.right = node3;
        Node node4 = new Node(4, node2);
        node2.left = node4;
        Node node5 = new Node(5, node2);
        node2.right = node5;
        Node node8 = new Node(8, node4);
        node4.left = node8;
        Node node9 = new Node(9, node5);
        node5.left = node9;
        Node node10 = new Node(10, node5);
        node5.right = node10;
        Node node6 = new Node(6, node3);
        node3.left = node6;
        Node node7 = new Node(7, node3);
        node3.right = node7;
        Node node11 = new Node(11, node7);
        node7.left = node11;
        Node node12 = new Node(12, node7);
        node7.right = node12;
        Node node13 = new Node(13, node11);
        node11.left = node13;
        Node node14 = new Node(14, node11);
        node11.right = node14;
        Node node15 = new Node(15, node12);
        node12.left = node15;
        Node node16 = new Node(16, node12);
        node12.right = node16;
        Node node17 = new Node(17, node16);
        Node node18 = new Node(18, node16);
        node16.left = node17;
        node16.right = node18;
        Node node19 = new Node(19, node18);
        node18.right = node19;
        node19.right = new Node(20, node19);

        List<Integer> inorderList = new ArrayList<Integer>();
        List<Integer> preorderList = new ArrayList<Integer>();
        printInOrderList(root, inorderList);
        printPreOrderList(root, preorderList);

        for (int i = 0; i < inorderList.size(); i++) {
            System.out.println(String.format("%-3d %-3d %-3d", i, inorderList.get(i), preorderList.get(i)));
        }

//        printInOrderIterative(root);
//        System.out.println("------------------------");
//        printInOrderIterativeBookSolution(root);
//        System.out.println("------------------------");
//        printInOrder(root);
    }

    static void printPreOrder(Node root) {
        if (root != null) {
            System.out.println(root.data);
            printPreOrder(root.left);
            printPreOrder(root.right);
        }
    }
    static void printInOrder(Node root) {
        if (root != null) {
            printInOrder(root.left);
            System.out.println(root.data);
            printInOrder(root.right);
        }
    }

    static void printPreOrderList(Node root, List<Integer> list) {
        if (root != null) {
            list.add(root.data);
            printPreOrderList(root.left, list);
            printPreOrderList(root.right, list);
        }
    }
    static void printInOrderList(Node root, List<Integer> list) {
        if (root != null) {
            printInOrderList(root.left, list);
            list.add(root.data);
            printInOrderList(root.right, list);
        }
    }

    static void printInOrderIterativeBookSolution(Node root) {
        Node curr = root, prev = null, next;
        int iter = 0;
        while (curr != null) {
            if (prev == null || prev.right == curr || prev.left == curr) {
                if (curr.left != null) {
                    next = curr.left;
                } else {
                    System.out.println(curr.data);
                    next = curr.right != null ? curr.right : curr.parent;
                }
            } else if (curr.left == prev) {
                System.out.println(curr.data);
                next = curr.right != null ? curr.right : curr.parent;
            } else { // curr.right == prev
                next = curr.parent;
            }
            prev = curr;
            curr = next;

            iter++;
        }
        System.out.println("Iterações: " + iter);
    }

    static void printInOrderIterative(Node root) {
        boolean rootPrinted = false;
        Node n = root;
        int iter = 0;
        while (!rootPrinted || n != root) {
            if (n.left != null) {
                n = n.left;
            } else {
                rootPrinted = printAndCheckRootPrinted(n, root, rootPrinted);
                if (n.right != null) {
                    n = n.right;
                } else {
                    Node parent = n.parent;
                    while (parent != null) {
                        if (isLeftChild(n, parent)) {
                            rootPrinted = printAndCheckRootPrinted(parent, root, rootPrinted);
                            if (parent.right != null) {
                                n = parent.right;
                                parent = null;
                            } else {
                                n = parent; parent = n.parent;
                            }
                        } else {
                            n = parent; parent = n.parent;
                        }
                        iter++;
                    }
                }
            }
            iter++;
        }

        System.out.println("Iterações: " + iter);
    }

    static boolean isLeftChild(Node n, Node parent) {
        if (n != null && parent != null) {
            return parent.left == n;
        }
        return false;
    }

    static boolean printAndCheckRootPrinted(Node n, Node root, boolean rootPrinted) {
        System.out.println(n.data);
        if (!rootPrinted) {
            return n == root;
        }
        return rootPrinted;
    }
}
