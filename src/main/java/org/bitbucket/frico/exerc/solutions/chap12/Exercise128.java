package org.bitbucket.frico.exerc.solutions.chap12;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Write a program to test whether the letters composing a string
 * can be permuted to form a palindrome. For example "edified" can be permuted
 * to "deified". Explore solutions that trade time for space.
 * Created by feliperico on 02/02/16.
 */
public class Exercise128 {


    public static void main(String[] args) {
        if (canBePalindrome("edified")) {
            System.out.println("SUCCESS");
        } else {
            System.out.println("FAILED!");
        }

        if (canBePalindrome2("edified")) {
            System.out.println("SUCCESS");
        } else {
            System.out.println("FAILED!");
        }
    }

    // O(N)
    static boolean canBePalindrome(String s) {
        Map<Character, Integer> hashCounter = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            if (hashCounter.containsKey(s.charAt(i))) {
                hashCounter.put(s.charAt(i), hashCounter.get(s.charAt(i)) + 1);
            } else {
                hashCounter.put(s.charAt(i), 1);
            }
        }

        int oddCounter = 0;
        for (Map.Entry<Character, Integer> entry : hashCounter.entrySet()) {
            if (entry.getValue() % 2 != 0) {
                oddCounter++;
                if (oddCounter > 1) break;
            }
        }

        return oddCounter <= 1;
    }

    // O(NlgN)
    static boolean canBePalindrome2(String s) {
        String ordered = getSorted(s);
        int oddCounter = 0, currCharCounter = 1;
        for (int i = 1; i < ordered.length() && oddCounter <= 1; i++) {
            if (ordered.charAt(i) != ordered.charAt(i - 1)) {
                if (currCharCounter % 2 != 0) {
                    oddCounter++;
                }
                currCharCounter = 1;
            } else {
                currCharCounter++;
            }
        }

        if (currCharCounter % 2 != 0) oddCounter++;

        return oddCounter <= 1;
    }

    static String getSorted(String s) {
        char[] array = s.toCharArray();
        Arrays.sort(array);
        return new String(array);
    }
}
