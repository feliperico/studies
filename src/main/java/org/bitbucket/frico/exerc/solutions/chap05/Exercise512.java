package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Created by feliperico on 08/09/15.
 *
 * Checking if rectangles intersect
 * Call a rectangle R whose sides are parallel to the x-axis and y-axis
 * xy-aligned. Such a rectangle is characterized by its left-most lower
 * point (Rx, Ry), it width Rw and its height Rh.
 *
 * Problem 5.12: Let R and S be xy-aligned rectangles in the Cartesian plane,
 * Write a function which tests if R and S have a nonempty intersection.
 * If the intersection is nonempty, return the rectangle formed by the
 * intersection.
 */
public class Exercise512 {


    static int[] getIntersection(int[] r, int[] s) {
        int[] xInter = getLinearIntersection(r[0], r[2], s[0], s[2]);
        if (xInter != null) {
            int[] yInter = getLinearIntersection(r[1], r[3], s[1], s[3]);
            if (yInter != null) {
                return new int[] {xInter[0], yInter[0], xInter[1], yInter[1]};
            }
        }
        return null;
    }


    static int[] getLinearIntersection(int rx, int rw, int sx, int sw) {
        if ((sx >= rx && sx <= (rx+rw)) || (rx >= sx && rx <= (sx+sw))) {
            int x = (sx >= rx) ? sx : rx;
            int xw = (rx+rw) >= (sx+sw) ? sx+sw : rx+rw;

            return new int[] {x, xw - x};
        }
        return null;
    }

}
