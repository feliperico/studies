package org.bitbucket.frico.exerc.solutions.chap16;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A social network consists of a set of individuals and, for each individual,
 * a list of his contacts. The contact relationship may not be symmetric - A may be a contact of B
 * but B may not be a contact of A. Define C to be an extended contact of A if he is either
 * a contact of A, or a contact of an extended contact of A.
 * Devise an efficient algorithm which takes a social network and computes for each individual its
 * extended contacts.
 *
 * Created by feliperico on 07/03/16.
 */
public class Exercise165 {

    static class User {
        int id;
        Collection<User> contacts, extendedContacts;
    }

    static Map<Integer, User> extendedContactsByDFS(User user, Map<Integer, User> extendedContacts) {
        for (User contact : user.contacts) {
            if (!extendedContacts.containsKey(contact.id)) {
                extendedContacts.put(contact.id, contact);
                extendedContactsByDFS(contact, extendedContacts);
            }
        }
        return extendedContacts;
    }

    static void computeExtendedContacts(List<User> users) {
        for (User u : users) {
            Map<Integer, User> extendedContactsMap = extendedContactsByDFS(u, new HashMap<Integer, User>());
            u.extendedContacts = extendedContactsMap.values();
        }
    }

}
