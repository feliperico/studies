package org.bitbucket.frico.exerc.solutions.chap13;

/**
 * What is the most effective sorting algorithm for each of the following situations:
 * a) a large array whose entries are random numbers
 * b) a small array of numbers
 * c) a large array of numbers that is already almost sorted
 * d) a large collection of integers that are drawn from a small range
 * e) a large collection of numbers most of which are duplicates
 * f) stability is required.
 *
 * a) Quick sort is the fastest in practice with large arrays, and offers an average performance of
 * O(NlogN) when the array entries are random numbers.
 * b) Insertion sort. Faster than quick sort for small arrays in practice.
 * c) Insertion sort. Almost linear performance when the array is almost sorted. Heap sort also good for
 * almost sorted arrays.
 * d) Counting sort, to count the number of positions that come before the current number.
 * e) Three way quick sort, also known as dutch flag algorithm. BST where the keys are the numbers and each
 * node also has the frequency of the number, then an in order traverse would give us the sorted array.
 * f) Insertion sort for small arrays and merge sort for large arrays.
 *
 * Created by feliperico on 23/03/16.
 */
public class Exercise131 {
}
