package org.bitbucket.frico.exerc.solutions.chap06;

/**
 * Created by feliperico on 17/09/15.
 *
 * Problem 6.14: Check whether a 9X9 2D representing a partially completed Sudoku
 * is valid. Specifically, check that no row, column, and 3X3 2D sub-array contains duplicates.
 * A 0-value in the 2D array indicates that the entry is blank; every other entry is in [1,9].
 */
public class Exercise614 {

    public static void main(String[] args) {
        int [] [] sudoku = new int[] [] {
                { 0, 8, 0, 4, 0, 2, 0, 6, 0 },
                { 0, 3, 4, 0, 0, 0, 9, 1, 0 },
                { 9, 6, 0, 0, 0, 3, 0, 8, 4 },
                { 0, 0, 0, 2, 1, 6, 0, 0, 0 },
                { 2, 0, 0, 0, 0, 9, 6, 0, 0 },
                { 0, 1, 0, 3, 5, 7, 0, 0, 8 },
                { 8, 4, 0, 0, 0, 0, 0, 7, 5 },
                { 0, 2, 6, 0, 0, 0, 1, 3, 0 },
                { 0, 9, 0, 7, 0, 1, 0, 4, 0 }
        };

        displayMatrix(sudoku);
        System.out.println(isValid(sudoku));

    }


    static boolean isValid(int[][] sudoku) {

        // checking rows
        for (int i = 0; i < 9; i++) {
            boolean[] presentNums = new boolean[10];
            for (int j = 0; j < 9; j++) {
                if (sudoku[i][j] != 0) {
                    if (!presentNums[sudoku[i][j]]) {
                        presentNums[sudoku[i][j]] = true;
                    } else {
                        return false;
                    }
                }
            }
        }

        //checking columns
        for (int j = 0; j < 9; j++) {
            boolean[] presentNums = new boolean[10];
            for (int i = 0; i < 9; i++) {
                if (sudoku[i][j] != 0) {
                    if (!presentNums[sudoku[i][j]]) {
                        presentNums[sudoku[i][j]] = true;
                    } else {
                        return false;
                    }
                }
            }
        }

        // checking 3x3 2D sub arrays
        for (int i = 0; i < 9; i += 3) {
            for (int j = 0; j < 9; j += 3) {
                boolean[] presentNums = new boolean[10];

                for (int x = i; x < i + 3; x++) {
                    for (int y = j; y < j + 3; y++) {
                        if (sudoku[x][y] != 0) {
                            if (!presentNums[sudoku[x][y]]) {
                                presentNums[sudoku[x][y]] = true;
                            } else {
                                return false;
                            }
                        }
                    }
                }

            }
        }

        return true;
    }


    private static void displayMatrix(int[][] array)
    {
        System.out.println("------------------------------------");
        for (int i = 0; i < array.length; i++)
        {
            if (i == 3 || i == 6)
                System.out.println("------------------------------------");
            for (int j = 0; j < array[i].length; j++)
            {
                System.out.format("%-3s", array[i][j]);
                if (j == 2 || j == 5 || j == 8)
                    System.out.print(" | ");
            }
            System.out.println();
        }
        System.out.println("------------------------------------");
    }
}
