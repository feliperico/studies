package org.bitbucket.frico.exerc.solutions.chap14;

/**
 * 14.5 Write a function that takes a BST T and a key k, and returns
 * the first entry larger than k that that would appears in a inorder walk.
 * If k is absent or no key larger than k is present, return null.
 * Created by feliperico on 28/01/16.
 */
public class Exercise145 {

    static class Node {
        int key;
        Node left, right;
    }


    static Node findKSuccessor(Node root, int k) {
        Node succ = null;
        boolean foundK = false;
        while (root != null) {
            if (root.key == k) {
                foundK = true;
                root = root.right;
            } else if (k < root.key) {
                succ = root;
                root = root.left;
            } else { //k > root.key
                root = root.right;
            }
        }

        return foundK ? succ : null;
    }
}
