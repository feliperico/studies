package org.bitbucket.frico.exerc.solutions.chap13;

import org.bitbucket.frico.algs.ThreeWayQuickSort;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Most sorting algorithms rely on a basic swap step. When records are of different lengths,
 * the swap step becomes nontrivial.
 * Sort lines of a text file that has one million lines such that the average length of a line
 * is 100 characters but the longest line is one million characters long.
 *
 * Created by feliperico on 23/03/16.
 */
public class Exercise132 {


    static void sortLines(File inputFile, File outputFile) {
        List<Integer> linePointers = new ArrayList<>();
        final List<String> lines = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            int lineCounter = 0;
            for(String line; (line = br.readLine()) != null; ) {
                // process the line.
                lines.add(line);
                linePointers.add(lineCounter++);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Integer[] pointers = new Integer[linePointers.size()];
        linePointers.toArray(pointers);
        ThreeWayQuickSort.sort(pointers, new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                if (lines.get(o1).length() > lines.get(o2).length()) {
                    return 1;
                } else if (lines.get(o1).length() < lines.get(o2).length()) {
                    return -1;
                }
                return 0;
            }
        });

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)))) {

            for (int i = 0; i < pointers.length; i++) {
                bw.write(lines.get(pointers[i]));
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

    }

}
