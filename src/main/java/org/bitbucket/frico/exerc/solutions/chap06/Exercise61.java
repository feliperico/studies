package org.bitbucket.frico.exerc.solutions.chap06;

/**
 * Created by feliperico on 17/09/15.
 *
 * Problem 6.1: write a function that takes an array A and an index i into A,
 * and rearrange the elements such that all elements less than A[i] appears first,
 * followed by elements equal to A[i], followed by elements greater than A[i].
 * Your algorithm should have O(1) space complexity and O(|A|) time complexity.
 */
public class Exercise61 {


    public static void main(String[] args) {
        printArray(dutchFlag(new int[]{1,7,8,9,5,4,16,86,25,8,5,89,5,75,41,5,6,3,2,5,89,5,4}, 4));
        printArray(dutchFlag(new int[]{5,7,8,9,1,4,16,86,25,8,5,89,5,75,41,5,6,3,2,5,89,5,4}, 0));
        int[] a = new int[]{1,7,8,9,4,4,16,86,25,8,5,89,5,75,41,5,6,3,2,5,89,5,5};
        printArray(dutchFlag(a, a.length - 1));

        printArray(dutchFlag2(new int[]{1, 7, 8, 9, 5, 4, 16, 86, 25, 8, 5, 89, 5, 75, 41, 5, 6, 3, 2, 5, 89, 5, 4}, 4));
        printArray(dutchFlag2(new int[]{5, 7, 8, 9, 1, 4, 16, 86, 25, 8, 5, 89, 5, 75, 41, 5, 6, 3, 2, 5, 89, 5, 4}, 0));
        a = new int[]{1,7,8,9,4,4,16,86,25,8,5,89,5,75,41,5,6,3,2,5,89,5,5};
        printArray(dutchFlag2(a, a.length - 1));
    }
    static int[] dutchFlag(int[] a, int index) {
        int middleValue = a[index];
        int middleStart = index, middleEnd = index;
        int i = 0;
        int iter = 0;
        while (i < a.length) {
            if (i < middleStart || i > middleEnd) {
                if (a[i] < middleValue && i > middleEnd) {
                    swap(a, i, middleStart++);
                    swap(a, i, ++middleEnd);
                } else if (a[i] == middleValue) {
                    swap(a, i, ++middleEnd);
                } else if (a[i] > middleValue && i < middleStart) {
                    swap(a, i, middleEnd--);
                    swap(a, i, --middleStart);
                } else {
                    i++;
                }
            } else {
                i++;
            }
            iter++;
        }

        System.out.println("iter=" + iter);
        return a;
    }

    static int[] dutchFlag2(int[] a, int index) {
        int pivot = a[index], middleStart = 0, middleEnd = a.length - 1, endLimit = 0;
        while (endLimit <= middleEnd) {
            if (a[endLimit] < pivot) {
                swap(a, endLimit++, middleStart++);
            } else if (a[endLimit] == pivot) {
                endLimit++;
            } else {
                swap(a, endLimit, middleEnd--);
            }
        }
        return a;
    }

    static void swap(int[] a, int from, int to) {
        if (to != from) {
            int temp = a[to];
            a[to] = a[from];
            a[from] = temp;
        }
    }

    static void printArray(int[] a) {
        String preffix = "[";
        for (int i = 0; i < a.length; i++) {
            System.out.print(preffix+a[i]);
            preffix = ", ";
        }
        System.out.println("]");
    }
}
