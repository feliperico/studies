package org.bitbucket.frico.exerc.solutions.chap13;

import org.bitbucket.frico.algs.SortingUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Suppose you need to reorder the elements of a very large array so that equal elements
 * appear together. If entries are integers, this can be done by sorting the array. If the
 * number of distinct integers is very small relative to the size of the array, an efficient
 * approach to sorting the array is to count the number of occurrences of each distinct integers and
 * write the appropriate number of each integer, in sorted order, to the array.
 * You are given an array of n Person objects. Each Person object has
 * a field key. Rearrange the elements of the array so that Person objects with equal keys
 * appear together. The order in which distinct keys appear is not important.
 * Your algorithm must run in O(N) time and O(K) additional space. How would your
 * solution change if keys have to appear in sorted order?
 *
 * Created by feliperico on 23/03/16.
 */
public class Exercise134 {

    static class Person {
        String name;
        int key;
        Person(String name, int key) {
            this.name = name;
            this.key = key;
        }
        @Override
        public String toString() {
            return this.name + "(" + this.key + ")";
        }
    }

    public static void main(String[] args) {
        Person[] persons = getEntry();
        rearrangeEqualKeys(persons);
        for (Person person : persons) {
            System.out.println(person);
        }
    }

    static Person[] getEntry() {
        Person[] persons = new Person[20];
        char intialName = 'A';
        Random random = ThreadLocalRandom.current();
        for (int i = 0; i < persons.length; i++) {
            persons[i] = new Person(Character.toString(intialName), random.nextInt(10));
            intialName++;
        }
        return persons;
    }

    static void rearrangeEqualKeys(Person[] persons) {
        Map<Integer, Integer> freq = new HashMap<>();
        for (Person p : persons) {
            if (freq.containsKey(p.key)) {
                int val = freq.get(p.key);
                freq.put(p.key, ++val);
            } else {
                freq.put(p.key, 1);
            }
        }

        Map<Integer, Integer> startOffset = new HashMap<>();
        int offSet = 0;
        for (Map.Entry<Integer, Integer> entry : freq.entrySet()) {
            if (!startOffset.containsKey(entry.getKey())) {
                startOffset.put(entry.getKey(), offSet);
                offSet += entry.getValue();
            }
        }

        while (!startOffset.isEmpty()) {
            int from = startOffset.get(startOffset.keySet().iterator().next());
            int toKey = persons[from].key;
            int to = startOffset.get(toKey);
            SortingUtils.swap(from, to, persons);
            int quantityLeft = freq.get(toKey) - 1;
            if (quantityLeft > 0) {
                freq.put(toKey, quantityLeft);
                startOffset.put(toKey, startOffset.get(toKey) + 1);
            } else {
                startOffset.remove(toKey);
            }
        }

    }

    static void rearrangeEqualKeysInSortedOrder(Person[] persons) {
        int[] freq = new int[persons.length];
        for (Person p : persons) {
            freq[p.key]++;
        }

        for (int i = 1; i < freq.length; i++) {
            freq[i] += freq[i - 1];
        }

        Person[] personsOut = new Person[persons.length];
        for (Person p : persons) {
            personsOut[--freq[p.key]] = p;
        }

        for (int i = 0; i < persons.length; i++) {
            persons[i] = personsOut[i];
        }

    }

}
