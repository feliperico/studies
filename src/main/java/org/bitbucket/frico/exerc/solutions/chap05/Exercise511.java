package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Created by feliperico on 02/09/15.
 * Enumerating primes
 * A natural number is called a prime if it is bigger than 1 and has no divisors
 * other than 1 and itself.
 *
 * Problem 5.11: Write a function that takes a single positive integer argument
 * n (n >= 2) and return all the primes between 1 and n.
 */
public class Exercise511 {


    public static void main(String[] args) {
        printPrimes(100);
    }

    static void printPrimes(int n) {
        for (int i = 1; i <= n; i++) {
            if (isPrime(i)) {
                if (i > 1) System.out.print(", ");
                System.out.print(i);
            }
        }
    }

    static boolean isPrime(int n) {
        if (n == 1) return true;

        if (n > 1) {
            boolean prime = true;
            for (int i = 2; i <= Math.ceil(Math.sqrt(n)); i++) {
                if ((n % i) == 0) {
                    prime = false;
                    break;
                }
            }
            return prime;
        }

        return false;
    }
}
