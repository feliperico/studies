package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Created by feliperico on 26/08/15.
 *
 * A 64-bit integer can be viewed as an array of 64 bits, with the bit at
 * index 0 corresponding to the least significant bit, and the bit at index 63
 * corresponding to the most significant bit. Implement code that takes as input
 * a 64-bit integer x and swaps the bits at indices i and j.
 */
class Exercise52 {

    /**
     * First verify if both bits differ from each other
     * (you only need to swap if they differ),
     * then make an exclusive or of the input with
     * a mask that contains 1-bit on i and j positions.
     *
     * @param input
     * @param i
     * @param j
     * @return
     */
    static long swapBits(long input, int i, int j) {
        if (i != j) {

            long bitI = (input>>i) & 1L;
            long bitJ = (input>>j) & 1L;

            if (bitI != bitJ) {
                input ^= ((1L << i) | (1L << j));
            }
        }

        return input;
    }
}
