package org.bitbucket.frico.exerc.solutions.chap09;

/**
 *
 * Created by feliperico on 07/01/16.
 */
public class Exercise93 {
    private static class Node {
        Node right, left;
        int data;
        Node (int data, Node left, Node right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
        @Override
        public String toString() {
            return String.format("[data=%d]", this.data);
        }
    }

    public static void main(String[] args) {
        System.out.println(isSymetric(getTree()));
    }

    static boolean isSymetric(Node root) {
        if (root != null) {
            return isSymetricHelper(root.left, root.right);
        }
        return true;
    }

    static boolean isSymetricHelper(Node a, Node b) {
        if (a != null && b != null) {
            return a.data == b.data
                    && isSymetricHelper(a.left, b.right)
                    && isSymetricHelper(a.right, b.left);
        } else if (a == null && b == null) {
            return true;
        } else { // (a == null && b != null) || (a != null && b == null)
            return false;
        }
    }

    static Node getTree() {
        return new Node(1, new Node(2, new Node(13, new Node(-7, null, null), null),new Node(45, new Node(-2,null,null), null)), new Node(2,new Node(45, null, new Node(-2, null, null)), new Node(13, null,new Node(-7, null, null))));
    }
}
