package org.bitbucket.frico.exerc.solutions.chap13;

import java.util.ArrayList;
import java.util.List;

/**
 * Given sorted arrays A and B of lengths n and m respectively, return
 * an array C containing elements common to A and B. The array C should be free of duplicates.
 * How would you perform this intersection if (1) n = m and (2) n << m.
 * Created by feliperico on 04/04/16.
 */
public class Exercise135 {

    // for cases when n << m
    static int[] getIntersection(int[] a, int[] b) {

        List<Integer> resultList = new ArrayList<>();
        int[] inner = a.length > b.length ? a : b;
        int[] outer = a.length > b.length ? b : a;

        for (int i = 0; i < outer.length; i++) {
            if (i == 0 || outer[i] != outer[i -1]) {
                if (binarySearch(inner, outer[i]) != -1) {
                    resultList.add(outer[i]);
                }
            }
        }

        int[] result = new int[resultList.size()];
        int counter = 0;
        for (Integer number : resultList) {
            result[counter++] = number;
        }

        return result;
    }

    // for cases that n is almost the same size as m
    static int[] getIntersection2(int[] a, int[] b) {
        int i = 0, j = 0;
        List<Integer> resultList = new ArrayList<>();
        while (i < a.length && j < b.length) {
            if (a[i] == b[j] && (i == 0 || a[i] != a[i - 1])) {
                resultList.add(a[i]);
            } else if (a[i] < b[j]) {
                i++;
            } else { // a[i] > b[j]
                j++;
            }
        }

        int[] result = new int[resultList.size()];
        int counter = 0;
        for (Integer number : resultList) {
            result[counter++] = number;
        }

        return result;
    }

    static int binarySearch(int[] array, int target) {
        int mid = array.length / 2;
        int start = 0;
        int end = array.length - 1;
        boolean found = false;
        int foundIdx = -1;

        while ((end - start + 1) >= 1 && !found) {
            if (array[mid] == target) {
                found = true;
                foundIdx = mid;
            } else if (target < array[mid]) {
                end = mid - 1;
            } else { // target > array[mid]
                start = mid + 1;
            }
            mid = start + ((end - start + 1) / 2);
        }

        return foundIdx;
    }

}
