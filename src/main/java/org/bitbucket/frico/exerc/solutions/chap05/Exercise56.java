package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * String and integer conversion
 * A string is a sequence of characters. A string may encode an integer,
 * e.g., "123" encodes 123. In this problem, you are to implement methods that
 * take a string representing an integer and return the corresponding integer,
 * and vice-versa.
 *
 * Your code should handle negative integers. It should throw an exception
 * if the string does not encode an integer, e.g. "123abc" is not a valid encoding.
 * Problem 5.6 Implement string/integer inter-conversion functions. Use the
 * following functions signatures: String intToString(int x) and int
 * stringToInt(String s).
 */
class Exercise56 {


    public static void main(String[] args) {
        System.out.println(intToString(123));
        System.out.println(stringToInt("123"));
        System.out.println(intToString(-123));
        System.out.println(stringToInt("-123"));
        System.out.println(intToString(1420));
        System.out.println(stringToInt("-1420"));
    }

    static String intToString(int x) {
        boolean negative = x < 0;
        if (negative) x *= -1;

        char zero = '0';
        String result = "";

        while (x != 0) {
            int rem = x % 10;
            result = (char)(zero + rem) + result;
            x /= 10;
        }

        if (result.isEmpty()) {
            return "0";
        }

        if (negative) result = "-" + result;

        return result;
    }

    static int stringToInt(String s) {
        boolean negative = s.startsWith("-");
        if (negative) s = s.substring(1, s.length());

        int result = 0;
        int multBase = 1;
        for (int i = s.length() - 1; i >= 0; i--) {
            char curr = s.charAt(i);
            if (!isDigit(curr)) throw new IllegalArgumentException("Argument does not encode a decimal number");
            result += ((s.charAt(i) - '0') * multBase);
            multBase *= 10;
        }

        if (negative) result *= -1;

        return result;
    }

    static boolean isDigit(char c) {
        return '0' <= c && c <= '9';
    }
}
