package org.bitbucket.frico.exerc.solutions.hr;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/kruskalmstrsub
 * Created by feliperico on 18/06/16.
 */
@SuppressWarnings("unchecked")
public class KruskalMST {

    static class Edge {
        int v1, v2, w;
        Edge(int v1,  int v2, int w) {
            this.v1 = v1;
            this.v2 = v2;
            this.w = w;
        }
        public int either() {
            return v1;
        }
        public int other(int v) {
            if (v == v1) return v2;
            return v1;
        }
    }

    static class WeightedGraph {
        List<Edge>[] adjacent;
        List<Edge> allEdges;
        int vertices;
        int edges;

        WeightedGraph(int vertices) {
            this.vertices = vertices;
            this.adjacent = (List<Edge>[]) new List[vertices];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new ArrayList<>();
            }
            allEdges = new LinkedList<>();
        }

        public void addEdge(Edge e) {
            int v = e.either(), w = e.other(v);
            adjacent[v].add(e);
            adjacent[w].add(e);
            allEdges.add(e);
        }

        public Iterable<Edge> adjacent(int v) {
            return adjacent[v];
        }
    }

    static class MinPQ {
        Edge[] array;
        int size;

        MinPQ(List<Edge> allEdges) {
            array = new Edge[allEdges.size() + 1];
            for (Edge e : allEdges) {
                add(e);
            }
        }

        private void swap(int i, int j) {
            Edge temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        private boolean greaterThanAnyChild(int i) {
            if (i * 2 <= array.length - 1
                    && array[i * 2] != null
                    && array[i].w > array[i * 2].w) {
                return true;
            }
            if ((i * 2) + 1 < array.length - 1
                    && array[(i * 2) + 1] != null
                    && array[i].w > array[(i * 2) + 1].w) {
                return true;
            }
            return false;
        }

        private void sink(int i) {
            while (i < size && greaterThanAnyChild(i)) {
                int child = i * 2;
                if (array[child + 1] != null && array[child + 1].w < array[child].w) child++;
                swap(i, child);
                i = child;
            }
        }

        private void swin(int i) {
            while (i > 1 && array[i/2].w > array[i].w) {
                swap(i, i /2);
                i /= 2;
            }
        }

        public void add(Edge e) {
            if (e != null) {
                array[++size] = e;
                swin(size);
            }
        }

        public Edge delMin() {
            if (size > 0) {
                Edge e = array[1];
                swap(1, size--);
                array[size + 1] = null;
                sink(1);
                return e;
            }
            return null;
        }

        public boolean isEmpty() {
            return size == 0;
        }
    }

    static class UF {
        int size[];
        int idx[];

        UF(int capacity) {
            size = new int[capacity];
            idx = new int[capacity];
            for (int i = 0; i < capacity; i++) {
                idx[i] = i;
                size[i] = 1;
            }
        }

        public boolean isConnected(int i, int j) {
            return root(i) == root(j);
        }

        private int root(int i) {
            while (idx[i] != i) {
                idx[i] = idx[idx[i]];
                i = idx[i];
            }
            return i;
        }

        public void connect(int i, int j) {
            int rootI = root(i);
            int rootJ = root(j);
            if (rootI == rootJ) return;
            if (size[rootI] < size[rootJ]) {
                idx[rootI] = rootJ;
                size[rootJ] += size[rootI];
            } else {
                idx[rootJ] = rootI;
                size[rootI] += size[rootJ];
            }
        }
    }

    static class KruskMST {
        int totalWeight;
        int mstEdges;

        KruskMST(WeightedGraph g, int source) {
            MinPQ pq = new MinPQ(g.allEdges);
            UF uf = new UF(g.vertices);
            while (!pq.isEmpty() && mstEdges < g.vertices - 1) {
                Edge e = pq.delMin();
                int v = e.either(), w = e.other(v);
                if (uf.isConnected(v, w)) continue;
                uf.connect(v, w);
                mstEdges++;
                totalWeight += e.w;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int vertices = scanIn.nextInt(), edges = scanIn.nextInt();
        WeightedGraph graph = new WeightedGraph(vertices);
        for (int i = 0; i < edges; i++) {
            int v1 = scanIn.nextInt(), v2 = scanIn.nextInt(), w = scanIn.nextInt();
            graph.addEdge(new Edge(v1 - 1, v2 - 1, w));
        }
        int source = scanIn.nextInt();

        System.out.println(getTotalWeight(graph, source));
    }

    static int getTotalWeight(WeightedGraph graph, int source) {
        KruskMST mst = new KruskMST(graph, source - 1);
        return mst.totalWeight;
    }
}
