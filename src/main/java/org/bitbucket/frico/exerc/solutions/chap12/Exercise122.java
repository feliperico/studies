package org.bitbucket.frico.exerc.solutions.chap12;

import java.util.Map;

/**
 * Created by feliperico on 23/01/16.
 */
public class Exercise122 {

    static final int PRIME = 31;

    public static void main(String[] args) {

    }

    static int getPosStateVal(char posState) {
        if (posState >= '0' && posState <= '9') {
            return posState - '0';
        } else if (posState >= 'A' && posState <= 'C') {
            return posState - 'A' + 10;
        }
        return -1;
    }

    static int calculateChessboardHash(String cbState) {
        int hash = 0;
        for (int i = 0; i < 64; i++) {
            hash += getPosStateVal(cbState.charAt(i)) * (int)Math.pow(PRIME, i);
        }
        return hash;
    }

    static int calculateChessboardHashIncrementally(int prevHash, Map<Integer, Character> moves) {
        int hash = prevHash;
        int[] calcAux = new int[moves.size()];
        int counter = 0, half = moves.size() / 2;
        for (Map.Entry<Integer, Character> e : moves.entrySet()) {
            int calc = getPosStateVal(e.getValue()) * (int)Math.pow(PRIME, e.getKey());
            if (counter < half) calc *= -1;
            calcAux[counter] = calc;
            counter++;
        }

        for (int i = 0; i < calcAux.length; i++) hash += calcAux[i];

        return hash;
    }
}
