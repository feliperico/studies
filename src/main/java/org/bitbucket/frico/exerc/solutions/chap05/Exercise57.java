package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Problem 5.7 Write a function that performs a base conversion.
 * Specifically, the input is an integer base b1, a string s, representing
 * an integer x in base b1, and another integer base b2. The output is the
 * string representing the integer x in base b2. Assume 2<=b1, b2<=16.
 * Use "A" ti represent 10, "B" for 11, ..., and "F" for 15.
 */
public class Exercise57 {
    static final char[] SYMBOLS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};


    public static void main(String[] args) {
        System.out.println(convertToBase(10, "100", 2));
        System.out.println(convertToBase(2, "100", 10));
        System.out.println(convertToBase(10, "100", 8));
    }

    static String convertToBase(int b1, String s, int b2) {
        boolean negative = s.startsWith("-");
        if (negative) s = s.substring(1, s.length());

        int numVal = 0;
        int baseMult = 1;

        for (int i = s.length() - 1; i >=0; i--) {
            char curr = s.charAt(i);
            numVal += getCharValue(curr) * baseMult;
            baseMult *= b1;
        }

        String result = "";
        while (numVal != 0) {
            result = SYMBOLS[numVal % b2] + result;
            numVal /= b2;
        }

        if (result.isEmpty()) return "0";

        if (negative) result = "-" + result;

        return result;
    }

    static int getCharValue(char c) {
        if ('0' <= c && c <= '9') {
            return c - '0';
        } else if ('A' <= c && c <= 'F') {
            return 10 + (c - 'A');
        } else {
            throw new IllegalArgumentException("Invalid argument");
        }
    }
}
