package org.bitbucket.frico.exerc.solutions.hr;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/primsmstsub
 *
 * Created by feliperico on 09/06/16.
 */
@SuppressWarnings("unchecked")
public class PrimsMST {

    static class Edge {
        int v1, v2, w;

        Edge(int v1, int v2, int w) {
            this.v1 = v1;
            this.v2 = v2;
            this.w = w;
        }

        public int either() {
            return v1;
        }

        public int other(int v) {
            if (v == v1) return v2;
            return v1;
        }
    }

    static class WeightedGraph {

        List<Edge>[] adjacent;
        int vertices;
        int edges;

        WeightedGraph(int vertices) {
            this.vertices = vertices;
            this.adjacent = (List<Edge>[]) new List[vertices];
            for (int i = 0; i < vertices; i++) {
                adjacent[i] = new ArrayList<>();
            }
        }

        public void addEdge(Edge edge) {
            int v = edge.either(), w = edge.other(v);
            adjacent[v].add(edge);
            adjacent[w].add(edge);
            edges++;
        }

        public Iterable<Edge> adjacent(int v) {
            return adjacent[v];
        }
    }



    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int vertices = scanIn.nextInt(), edges = scanIn.nextInt();
        WeightedGraph graph = new WeightedGraph(vertices);
        for (int i = 0; i < edges; i++) {
            int v1 = scanIn.nextInt(), v2 = scanIn.nextInt(), w = scanIn.nextInt();
            graph.addEdge(new Edge(v1 - 1, v2 - 1, w));
        }
        int source = scanIn.nextInt();

        System.out.println(getTotalWeight(graph, source));
    }

    static int getTotalWeight(WeightedGraph graph, int source) {
        PrimMST mst = new PrimMST(graph, source - 1);
        return mst.weight;
    }

    static class PrimMST {

        private boolean[] marked;
        private MinPQ pq;
        private int weight;

        PrimMST(WeightedGraph g, int source) {
            pq = new MinPQ(g.edges);
            marked = new boolean[g.vertices];

            visit(g, source);
            while (!pq.isEmpty()) {
                Edge e = pq.delMin();
                int v = e.either(), w = e.other(v);
                if (marked[v] && marked[w]) continue;
                weight += e.w;
                if (!marked[v]) visit(g, v);
                if (!marked[w]) visit(g, w);
            }
        }

        private void visit(WeightedGraph g, int v) {
            marked[v] = true;
            for (Edge e : g.adjacent(v)) {
                if (!marked[e.other(v)]) {
                    pq.add(e);
                }
            }
        }

    }

    static class MinPQ {
        private Edge[] array;
        private int size;

        MinPQ(int capacity) {
            array = new Edge[capacity + 1];
            size = 0;
        }

        private void sink(int i) {
            while (i * 2 <= size && greaterThanAnyChild(i)) {
                int child = i * 2;
                if (array[child + 1] != null && array[child + 1].w < array[child].w) child++;
                swap(i, child);
                i = child;
            }
        }

        private void swin(int i) {
            while (i > 1 && array[i].w < array[i/2].w) {
                swap(i, i/2);
                i /= 2;
            }
        }

        public void add(Edge e) {
            if (size < array.length) {
                array[++size] = e;
                swin(size);
            }
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Edge delMin() {
            if (size != 0) {
                Edge e = array[1];
                swap(1, size--);
                array[size + 1] = null;
                sink(1);
                return e;
            }
            return null;
        }


        private boolean greaterThanAnyChild(int i) {
            if (array[i].w > array[i*2].w
                    || (array[(i*2) + 1] != null && array[i].w > array[(i*2) + 1].w)){
                return true;
            }
            return false;
        }

        private void swap(int i, int j) {
            Edge temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
}

