package org.bitbucket.frico.exerc.solutions.hr;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.util.stream.IntStream;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[5];
        for(int arr_i=0; arr_i < 5; arr_i++){
            arr[arr_i] = in.nextInt();
        }

        long[] minMax = getMinMax(arr);

        System.out.println(minMax[0] + " " + minMax[1]);
    }


    static long[] getMinMax(int[] numbers) {
        long min = 0, max = 0;
        for (int i = 0; i < numbers.length; i++) {
            final int excludedIdx = i;
            int[] numbersToSum = IntStream.range(0, numbers.length)
                    .filter(idx -> idx != excludedIdx)
                    .map(idx -> numbers[idx]).toArray();
            long sum = Arrays.stream(numbersToSum).mapToLong(intNum -> (long) intNum).sum();
            if (min == max && min == 0) {
                min = max = sum;
            } else {
                if (sum < min) {
                    min = sum;
                }
                if (sum > max) {
                    max = sum;
                }
            }
        }

        return new long[] {min, max};
    }

    static int birthdayCakeCandles(int n, int[] ar) {
        int tallest = 1;
        int tallestCounter = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > tallest) {
                tallest = ar[i];
                tallestCounter = 1;
            } else if (ar[i] == tallest) {
                tallestCounter++;
            }
        }
        return tallestCounter;
    }

}
