package org.bitbucket.frico.exerc.solutions.chap14;

/**
 * How would you build a BST of minimum possible height from a sorted array?
 * - RedBlack
 * - recursive construction using the center of the array as the root.
 *
 * Created by feliperico on 11/02/16.
 */
public class Exercise147 {

    static class Node {
        Node left, right;
        int key;
        Node (int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};
        Node root = buildBSTFromSortedArray(array);

        printInOrder(root);

    }

    static void printInOrder(Node node) {
        if (node != null) {
            printInOrder(node.left);
            System.out.println(node.key);
            printInOrder(node.right);
        }
    }
    static Node buildBSTFromSortedArray(int[] array) {
        return buildBSTFromSortedArray(array, 0, array.length);
    }

    static Node buildBSTFromSortedArray(int[] array, int start, int end) {
        if (start < end) {
            int mid = start + ((end - start) / 2);
            return new Node(array[mid], buildBSTFromSortedArray(array, start, mid), buildBSTFromSortedArray(array, mid + 1, end));
        }
        return null;
    }
}
