package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * BIT Reversal
 * Here is a bit fiddling problem that is concerned with restructuring.
 *
 * Problem 5.3: Write a function that takes a 64-bit integer x and returns
 * a 64-bit integer consisting of the bits of x in reverse order.
 *
 *
 */
public class Exercise53 {


    public static void main(String[] args) {
        printBinary(164);
        long reversed = reverseBits(164);
        printBinary(reversed);
    }

    static long reverseBits(long x) {
        for (int i = 63; i >= 31; i--) {
            x = swapBits(x, i, 63 - i);
        }
        return x;
    }

    static void printBinary(long x) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Long.numberOfLeadingZeros(x); i++) {
            sb.append("0");
        }
        sb.append(Long.toBinaryString(x));
        System.out.println(sb.toString());
    }

    static long swapBits(long x, int i, int j) {
        if (i != j) {
            long bitI = (x>>>i) & 1L;
            long bitJ = (x>>>j) & 1L;
            if (bitI != bitJ) {
                x ^= ((1L << i)|(1L << j));
            }
        }
        return x;
    }

}
