package org.bitbucket.frico.exerc.solutions.chap14;

/**
 * Created by feliperico on 06/02/16.
 */
public class Exercise144 {

    static class Node {
        Node left, right;
        int key;
        Node(int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        Node root = getRoot();
        Node root2 = root;

        Node found1 = findRecursive(108, root);
        Node found2 = findIterative(108, root2);

        if (found1 == found2) {
            System.out.println("SUCCESS");
        } else {
            System.out.println("FAILED");
        }

    }

    static Node getRoot() {
        return new Node(108, new Node(108, new Node(-10, new Node(-14, null, null), new Node(2, null, null)), new Node(108, null, null)), new Node(285, new Node(243, null, null), new Node(285, null, new Node(401, null,  null))));
    }

    static Node findRecursive(int k, Node root) {
        if (root != null) {
            Node found = null;
            if (k <= root.key) found = findRecursive(k, root.left);
            if (found == null && k == root.key) found = root;
            if (found == null && k < root.key) found = findRecursive(k, root.right);
            return found;
        }
        return null;
    }

    static Node findIterative(int k, Node root) {
        while (root != null) {
            if (root.left != null) {
                Node pre = root.left;
                while (pre.right != null && pre.right != root) pre = pre.right;

                if (pre.right == null) {
                    pre.right = root;
                    root = root.left;
                } else { //pre.right == root
                    pre.right = null;
                    if (root.key == k) return root;
                    root = root.right;
                }

            } else {
                if (root.key == k) return root;
                root = root.right;
            }
        }
        return null;
    }

}
