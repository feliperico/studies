package org.bitbucket.frico.exerc.solutions.chap14;

/**
 * 14.1 Write a function that takes as input the root of a binary tree
 * whose nodes have a key field, and returns true iff the tree satisfies the
 * BST property.
 *
 * Created by feliperico on 27/01/16.
 */
public class Exercise141 {

    static class Node {
        Node left, right;
        int key;
    }


    static boolean checkBST(Node node) {
        boolean isBST = true;
        if (node != null) {
            isBST = checkBST(node.left);
            boolean isLeftBST = node.left == null || node.left.key <= node.key;
            boolean isRightBST = node.right == null || node.key <= node.right.key;
            isBST = isBST && isLeftBST &&isRightBST;
            if (isBST) isBST = checkBST(node.right);
        }
        return isBST;
    }

}
