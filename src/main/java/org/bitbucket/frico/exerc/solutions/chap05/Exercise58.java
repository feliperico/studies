package org.bitbucket.frico.exerc.solutions.chap05;

/**
 * Widely deployed spreadsheets use an alphabetical encoding of the successive
 * columns. Specifically, consecutive columns are identified by "A", "B", "C",...,
 * "X", "Y", "Z", "AA", "AB", ..., "ZZ", "AAA", "AAB",...
 * Problem 5.8 Write a function that converts Excel column ids to the corresponding
 * integer, with "A" corresponding to 1. The function signature is
 * int decodeColID(String s); you may ignore conditions, such as col containing
 * characters outside of [A, Z]. How would you test your code?
 */
public class Exercise58 {


    public static void main(String[] args) {
        System.out.println(decodeColumnId("A"));
        System.out.println(decodeColumnId("B"));
        System.out.println(decodeColumnId("C"));
        System.out.println(decodeColumnId("D"));
        System.out.println(decodeColumnId("AA"));
        System.out.println(decodeColumnId("AB"));
        System.out.println(decodeColumnId("AC"));
        System.out.println(decodeColumnId("AD"));
        System.out.println(decodeColumnId("BA"));
        System.out.println(decodeColumnId("BB"));
        System.out.println(decodeColumnId("BC"));
        System.out.println(decodeColumnId("BD"));
    }

    static int decodeColumnId(String strId) {
        int result = 0;
        int baseMult = 1;
        for (int i = strId.length() - 1; i >= 0; i--) {
            int currVal = (strId.charAt(i) - 'A' + 1);
            result += (currVal * baseMult);
            baseMult *= 26;
        }

        return result;
    }

}
