package org.bitbucket.frico.exerc.solutions.chap13;

import org.bitbucket.frico.algs.HeapSort;

import java.util.HashMap;
import java.util.Map;

/**
 * Count the occurrences of characters in a sentence
 * Computers are ideally suited to taking a large amount of data and summarizing it,
 * e.g. as gross statics.
 * Given a string s, print in alphabetical order each character that appears
 * in s, and the number of times that it appears. For example, if s = 'bcdacebe', output
 * "(a,1), (b,2), (c,2), (d,1), (e,2)"
 *
 * Created by feliperico on 04/04/16.
 */
public class Exercise137 {

    static class CharacterCounter implements Comparable<CharacterCounter>{
        Character character;
        int occurrences;

        CharacterCounter(Character character) {
            this.character = character;
            this.occurrences = 1;
        }

        @Override
        public int compareTo(CharacterCounter o) {
            return character.compareTo(o.character);
        }
        @Override
        public String toString() {
            return "(" + character + "," + occurrences + ")";
        }
    }

    public static void main(String[] args) {
        printCharactersOccurrences("bcdacebe");
    }

    static void printCharactersOccurrences(String string) {
        Map<Character, CharacterCounter> hashMap = new HashMap<>();
        for (int i = 0; i < string.length(); i++) {
            char curr = string.charAt(i);
            if (hashMap.containsKey(curr)) {
                hashMap.get(curr).occurrences++;
            } else {
                hashMap.put(curr, new CharacterCounter(curr));
            }
        }

        CharacterCounter[] characterCounters = new CharacterCounter[hashMap.size()];
        int idx = 0;
        for (Map.Entry<Character, CharacterCounter> entry : hashMap.entrySet()) {
            characterCounters[idx++] = entry.getValue();
        }
        HeapSort.sort(characterCounters);

        String prefix = "";
        for (CharacterCounter cc : characterCounters) {
            System.out.print(prefix + cc);
            prefix = ", ";
        }

    }

}
