package org.bitbucket.frico.exerc.solutions.chap14;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Which traversal orders (preorder, inorder or postorder) of BST
 * can be used to reconstruct the BST uniquely? Write a program that takes as input
 * a sequence of node keys and computes the corresponding BST. Assume that all keys are unique.
 *
 * Created by feliperico on 06/02/16.
 */
public class Exercise1412 {

    private static class Node {
        Node left, right;
        int key;
        Node (int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        Node root = getTree();
        ArrayList<Integer> preorderList = new ArrayList<>();
        preorderSeq(root, preorderList);
        Integer[] preorder = new Integer[preorderList.size()];
        preorder = preorderList.toArray(preorder);
        Node newRoot = getBST(preorder);

        if (checkEqual(root, newRoot)) {
            System.out.println("SUCCESS!");
        } else {
            System.out.println("FAILED!");
        }

    }

    static boolean checkEqual(Node root1, Node root2) {
        if (root1 != null && root2 != null) {
            if (root1.key == root2.key) {
                boolean equal = true;
                equal = checkEqual(root1.left, root2.left);
                if (equal) equal = checkEqual(root1.right, root2.right);

                return equal;
            }
        } else if (root1 == null && root2 == null) {
            return true;
        }
        return false;
    }

    static Node getTree() {
        return new Node(19, new Node(7, new Node(3, new Node(2,null,null), new Node(5, null, null)), new Node(11, null, new Node(17, new Node(13, null, null), null))), new Node(43, new Node(23, null, new Node(37, new Node(29, null, new Node(31, null, null)), new Node(41, null, null))), new Node(47, null, new Node(53, null, null))));
    }

    static void preorderSeq(Node root, List<Integer> preorder) {
        if (root !=  null) {
            preorder.add(root.key);
            preorderSeq(root.left, preorder);
            preorderSeq(root.right, preorder);
        }
    }

    static Node getBST(int start, int limit, Integer[] preorder) {
        int lStart = start + 1, rStart = start + 1;
        Node left = null, right = null;
        while (rStart < limit && preorder[rStart] < preorder[start]) {
            rStart++;
        }

        if (lStart < limit && preorder[lStart] < preorder[start]) {
            left = getBST(lStart, rStart, preorder);
        }

        if (rStart < limit && preorder[rStart] > preorder[start]) {
            right = getBST(rStart, limit, preorder);
        }

        return new Node(preorder[start], left, right);
    }

    static Node getBST(Integer[] preorder) {
        return getBST(0, preorder.length, preorder);
    }

}
