package org.bitbucket.frico.exerc.solutions.chap16;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a 2D array of black and white entries representing a maze with
 * designated entrance and exit points, find a path from the entrance to the exit
 * if such a path exists.
 *
 * Created by feliperico on 23/02/16.
 */
public class Exercise161 {

    static class Coord {
        int x, y;
        Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }
        @Override
        public boolean equals(Object o) {
            if (o == null) return false;
            if (o instanceof Coord) {
                Coord coord = (Coord)o;
                return this.x == coord.x && this.y == coord.y;
            }
            return false;
        }
    }


    static boolean searchMazeDfs(int[][] maze, Coord curr, Coord exit, List<Coord> path) {
        if (curr.equals(exit)) {
            return true;
        }

        for (Coord shift : getShiftList()) {
            Coord next = new Coord(curr.x + shift.x, curr.y + shift.y);
            if (isFeasible(next, maze)) {
                maze[next.x][next.y] = 1;
                path.add(next);
                if (searchMazeDfs(maze, next, exit, path)) {
                    return true;
                }
                path.remove(path.size() - 1);
            }
        }

        return false;
    }

    static boolean isFeasible(Coord coord, int[][] maze) {
        return coord.x >= 0 && coord.x < maze.length
                && coord.y >= 0 && coord.y < maze[0].length
                && maze[coord.x][coord.y] == 0;
    }

    static List<Coord> getShiftList() {
        List<Coord> shift = new ArrayList<>();
        shift.add(new Coord(0, 1));
        shift.add(new Coord(0, -1));
        shift.add(new Coord(1, 0));
        shift.add(new Coord(-1, 0));
        return shift;
    }

}
