package org.bitbucket.frico.exerc.solutions.chap12;

/**
 * Design a hash function that is suitable for words in a dictionary.
 *
 * Created by feliperico on 28/01/16.
 */
public class Exercise121 {


    static int hash(String word) {
        int hash = 0;
        char[] wordArr = word.toCharArray();
        for (int i = 0; i < wordArr.length; i++) {
            hash += wordArr[i] * (int)Math.pow(31, wordArr.length - (i + 1));
        }

        return hash;
    }
}
