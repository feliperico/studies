package org.bitbucket.frico.exerc.solutions.hr;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/even-tree
 * Created by feliperico on 22/06/16.
 *
 *
 */
@SuppressWarnings("unchecked")
public class EvenTree {
/*
30 29
2 1
3 2
4 3
5 2
6 4
7 4
8 1
9 5
10 4
11 4
12 8
13 2
14 2
15 8
16 10
17 1
18 17
19 18
20 4
21 15
22 20
23 2
24 12
25 21
26 17
27 5
28 27
29 4
30 25
--
11
=====
40 39
2 1
3 2
4 3
5 2
6 5
7 1
8 3
9 7
10 1
11 10
12 1
13 9
14 7
15 7
16 13
17 13
18 16
19 1
20 3
21 5
22 14
23 15
24 3
25 17
26 13
27 25
28 27
29 8
30 14
31 15
32 15
33 19
34 18
35 11
36 14
37 19
38 29
39 12
40 30
---
10
 */
    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int n = scanIn.nextInt(), m = scanIn.nextInt();
        Graph graph = new Graph(n);
        for (int i = 0; i < m; i++) {
            int v1 = scanIn.nextInt(), v2 = scanIn.nextInt();
            graph.addEdge(v1 - 1, v2 - 1);
        }
        scanIn.close();
        //graph.print();
        System.out.println(calculateRemovableEdges(graph));
    }

    static class Graph {
        ArrayList<Integer>[] adjacent;
        int vertices, edges;

        public Graph(int vertices) {
            this.vertices = vertices;
            this.adjacent = (ArrayList<Integer>[]) new ArrayList[vertices];
            for (int i = 0; i < vertices; i++) {
                adjacent[i] = new ArrayList<>();
            }
        }

        public void addEdge(int v,int w) {
            adjacent[v].add(w);
            adjacent[w].add(v);
            edges++;
        }

        public void removeEdge(int v, int w) {
            adjacent[v].remove(Integer.valueOf(w));
            adjacent[w].remove(Integer.valueOf(v));
            edges--;
        }

        public Iterable<Integer> cloneAdjacent(int v) {
            return (ArrayList<Integer>)adjacent[v].clone();
        }

        public void print() {
            boolean[] marked = new boolean[vertices];
            Queue<Integer> q = new LinkedList<>();
            q.add(0);
            while (!q.isEmpty()) {
                int root = q.remove();
                for (int v : adjacent[root]) {
                    if (!marked[v]) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(root).append(" -> ").append(v);
                        System.out.println(sb.toString());
                        q.add(v);
                    }
                }
                marked[root] = true;
            }
        }
    }

    static int calculateRemovableEdges(Graph graph) {
        int[] subtreeSize = new int[graph.vertices];
        countSubTreeSizes(graph, subtreeSize);
        //printSubtreeSize(subtreeSize);
        boolean[] pastRoot = new boolean[graph.vertices];

        int initialEdges = graph.edges;
        Queue<Integer> roots = new LinkedList<>();
        roots.add(0);

        while (!roots.isEmpty()) {
            int root = roots.remove();
            for (int v : graph.cloneAdjacent(root)) {
                if (graph.adjacent[root].size() > 1
                        && !pastRoot[v]
                        && subtreeSize[v] % 2 == 0) {
                    graph.removeEdge(root, v);
                    subtreeSize[root] -= subtreeSize[v];
                    roots.add(v);
                }
            }
            for (int v : graph.adjacent[root]) {
                if (!pastRoot[v]) {
                    roots.add(v);
                }
            }
            pastRoot[root] = true;
        }


        return initialEdges - graph.edges;
    }

    static void countSubTreeSizes(Graph graph, int[] subtreeSize) {
        countSubTreeSize(graph, 0, 0, subtreeSize);
    }

    static void countSubTreeSize(Graph graph, int current, int parent, int[] subtreeSize) {
        int sum = 1;
        for (int vertex : graph.adjacent[current]) {
            if (vertex != parent) {
                countSubTreeSize(graph, vertex, current, subtreeSize);
                sum += subtreeSize[vertex];
            }
        }
        subtreeSize[current] = sum;
    }

    static void printSubtreeSize(int[] subtreeSize) {
        for (int i = 0; i < subtreeSize.length; i++) {
            System.out.println(String.format("[%d]=%d", i, subtreeSize[i]));
        }
    }

}
