package org.bitbucket.frico.ds;

import java.util.*;

/**
 * Grafo
 * Created by feliperico on 17/02/16.
 */
public class Graph {

    protected List<Integer>[] vertices;
    protected int numberOfVertices;
    protected int numberOfEdges;

    @SuppressWarnings("unchecked")
    public Graph(int v) {
        numberOfVertices = v;
        vertices = (List<Integer>[]) new List[numberOfVertices];
        for (int i = 0; i < v; i++) {
            vertices[i] = new ArrayList<>();
        }
    }

    public void addEdge(int v, int w) {
        if (v >= 0 && w >=0 && v < numberOfVertices && w < numberOfVertices) {
            vertices[v].add(w);
            vertices[w].add(v);
            numberOfEdges++;
        }
    }

    public Iterable<Integer> adjacent(int v) {
        if (v >= 0 && v < numberOfVertices) {
            return vertices[v];
        }
        return Collections.emptyList();
    }

    public int numberOfVertices() {
        return numberOfVertices;
    }

    public int numberOfEdges() {
        return numberOfEdges;
    }

    public int degree(int v) {
        int degree = 0;
        for (int w : adjacent(v)) degree++;
        return degree;
    }

    public int maxDegree() {
        int max = 0;
        for (int v = 0; v < numberOfVertices; v++) {
            int degree = degree(v);
            if (degree > max) max = degree;
        }
        return max;
    }

    public int avgDegree() {
        return 2 * numberOfEdges / numberOfVertices;
    }

    public int numberOfSelfLoops() {
        int counter = 0;
        for (int v = 0; v < numberOfVertices; v++) {
            for (int w : adjacent(v)) {
                if (w == v) counter++;
            }
        }
        return counter / 2;
    }

    @Override
    public String toString()
    {
        String s = numberOfVertices + " vertices, " + numberOfEdges + " edges\n";
        for (int v = 0; v < numberOfVertices; v++)
        {
            s += v + ": ";
            for (int w : this.adjacent(v)) {
                s += w + " ";
            }
            s += "\n";
        }
        return s;
    }
}
