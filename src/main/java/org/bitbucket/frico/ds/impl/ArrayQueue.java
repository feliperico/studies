package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;

import java.util.Iterator;

/**
 * Created by feliperico on 22/10/15.
 */
public class ArrayQueue<Item> implements Queue<Item> {
    private static final int INITIAL_CAPACITY = 10;

    private int firstIndex = -1;
    private int lastIndex = -1;
    private Item[] items;

    @SuppressWarnings("unchecked")
    public ArrayQueue() {
        items = (Item[]) new Object[INITIAL_CAPACITY];
    }

    @Override
    public void enqueue(Item item) {
        if (size() == items.length) {
            resize(items.length * 2);
        } else if (lastIndex == items.length - 1) {
            rearrangeItems();
        }
        items[++lastIndex] = item;
        if(firstIndex == -1) {
            firstIndex = lastIndex;
        }
    }

    @Override
    public Item dequeue() {
        if (!isEmpty()) {
            Item item = items[firstIndex];
            items[firstIndex++] = null;
            if (lastIndex < firstIndex) {
                firstIndex = lastIndex = -1;
            } else if (size() < items.length / 4) {
                resize(items.length / 2);
            }
            return item;
        }
        return null;
    }

    @Override
    public Item first() {
        if (!isEmpty()) return items[firstIndex];
        return null;
    }

    @Override
    public boolean isEmpty() {
        return firstIndex == -1;
    }

    @Override
    public int size() {
        if (firstIndex == -1) return 0;
        return lastIndex - firstIndex + 1;
    }

    @SuppressWarnings("unchecked")
    private void resize(int newCapacity) {
        rearrangeItems();
        int limit = newCapacity > items.length ? items.length - 1 : newCapacity - 1;
        Item[] newItems = (Item[]) new Object[newCapacity];
        System.arraycopy(items, 0, newItems, 0, limit + 1);
        items = newItems;
    }

    private void rearrangeItems() {
        if (firstIndex > 0) {
            int j = 0;
            for (int i = firstIndex; i <= lastIndex; i++, j++) {
                items[j] = items[i];
            }
            int newLastIndex = lastIndex - firstIndex;
            for (int i = newLastIndex + 1; i < items.length; i++) items[i] = null;
            firstIndex = 0;
            lastIndex = newLastIndex;
        }
    }

    @Override
    public Iterator<Item> iterator() {
        return new AQueueIterator();
    }

    private class AQueueIterator implements Iterator<Item> {

        private int currIndex = firstIndex;

        @Override
        public boolean hasNext() {
            return currIndex != -1 && currIndex <= lastIndex;
        }

        @Override
        public Item next() {
            if (hasNext()) {
                return items[currIndex++];
            }
            return null;
        }

        @Override
        public void remove() {
        }
    }
}
