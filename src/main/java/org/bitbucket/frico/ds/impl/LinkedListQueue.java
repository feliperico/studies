package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;

import java.util.Iterator;

/**
 * Created by feliperico on 22/10/15.
 */
public class LinkedListQueue<Item> implements Queue<Item> {

    private Node first;
    private Node last;
    private int currSize;

    @Override
    public void enqueue(Item item) {
        if (item != null) {
            Node oldLast = last;
            last = new Node();
            last.item = item;
            if (oldLast != null) oldLast.next = last;
            if (first == null) first = last;
            currSize++;
        }
    }

    @Override
    public Item dequeue() {
        if (first != null) {
            Node oldFirst = first;
            first = first.next;
            oldFirst.next = null;
            currSize--;
            if (isEmpty()) last = null;
            return oldFirst.item;
        }
        return null;
    }

    @Override
    public Item first() {
        if (first != null) {
            return first.item;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return currSize == 0;
    }

    @Override
    public int size() {
        return currSize;
    }

    @Override
    public Iterator<Item> iterator() {
        return new LLQueueIterator();
    }

    private class Node {
        Item item;
        Node next;
    }

    private class LLQueueIterator implements Iterator<Item> {

        Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (current != null) {
                Node oldCurr = current;
                current = current.next;
                return oldCurr.item;
            }
            return null;
        }

        @Override
        public void remove() {
        }
    }
}
