package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;
import org.bitbucket.frico.ds.SymbolTable;

import java.util.Iterator;

/**
 * Created by feliperico on 09/11/15.
 */
public class RedBlackBSTSymbolTable<Key extends Comparable<Key>, Val> implements SymbolTable<Key, Val> {

    private Node root;

    @Override
    public void put(Key key, Val value) {
        root = put(root, key, value);
        root.color = Color.BLACK;
    }

    private Node put(Node node, Key key, Val val) {
        if (node == null) {
            node =  new Node(key, val);
        } else {
            int comp = key.compareTo(node.key);
            if (comp == 0) {
                node.val = val;
            } else if (comp < 0) {
                node.left = put(node.left, key, val);
            } else { /* comp > 0 */
                node.right = put(node.right, key, val);
            }

            if (node.right != null && node.right.isRed()) {
                node = rotateLeft(node);
            }
            if (node.left != null && node.left.left != null
                    && node.left.isRed() && node.left.left.isRed()) {
                node = rotateRight(node);
            }
            if (node.left != null && node.right != null
                    && node.left.isRed() && node.right.isRed()) {
                flipColors(node);
            }
        }

        node.count = 1 + size(node.left) + size(node.right);

        return node;
    }

    @Override
    public boolean contains(Key key) {
        return search(root, key) != null;
    }

    private Node search(Node node, Key key) {
        if (node == null) return null;
        int comparison = key.compareTo(node.key);
        if (comparison == 0) {
            return node;
        } else if (comparison < 0) {
            return search(node.left, key);
        } else {
            return search(node.right, key);
        }
    }

    @Override
    public Val get(Key key) {
        Node node = search(root, key);
        if (node != null) return node.val;
        return null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        return size(root);
    }

    @Override
    public void delete(Key key) {
        if (!contains(key)) {
            System.err.println("symbol table does not contain " + key);
            return;
        }

        // if both children of root are black, set root to red
        if (!isRed(root.left) && !isRed(root.right))
            root.color = Color.RED;

        root = delete(root, key);

        if (!isEmpty()) root.color = Color.BLACK;
    }

    // delete the key-value pair with the given key rooted at h
    private Node delete(Node h, Key key) {
        // assert get(h, key) != null;

        if (key.compareTo(h.key) < 0)  {
            if (!isRed(h.left) && !isRed(h.left.left))
                h = moveRedLeft(h);
            h.left = delete(h.left, key);
        } else {
            if (isRed(h.left))
                h = rotateRight(h);
            if (key.compareTo(h.key) == 0 && h.right == null && h.left == null)
                return null;
            if (h.right != null && !isRed(h.right) && !isRed(h.right.left))
                h = moveRedRight(h);
            if (key.compareTo(h.key) == 0) {
                Node x = min(h.right);
                h.key = x.key;
                h.val = x.val;
                // h.val = get(h.right, min(h.right).key);
                // h.key = min(h.right).key;
                h.right = deleteMin(h.right);
            }
            else h.right = delete(h.right, key);
        }
        return balance(h);
    }

    // delete the key-value pair with the minimum key rooted at h
    private Node deleteMin(Node h) {
        if (h.left == null)
            return null;

        if (!isRed(h.left) && !isRed(h.left.left))
            h = moveRedLeft(h);

        h.left = deleteMin(h.left);
        return balance(h);
    }

    // the smallest key in subtree rooted at x; null if no such key
    private Node min(Node x) {
        // assert x != null;
        if (x != null) {
            if (x.left == null) return x;
            else return min(x.left);
        }
        return null;
    }

    // Assuming that h is red and both h.left and h.left.left
    // are black, make h.left or one of its children red.
    private Node moveRedLeft(Node h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

        flipColors(h);
        if (h.right != null && isRed(h.right.left)) {
            h.right = rotateRight(h.right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    // Assuming that h is red and both h.right and h.right.left
    // are black, make h.right or one of its children red.
    private Node moveRedRight(Node h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
        flipColors(h);
        if (h.left != null && isRed(h.left.left)) {
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }

    // restore red-black tree invariant
    private Node balance(Node h) {
        // assert (h != null);

        if (isRed(h.right))
            h = rotateLeft(h);
        if (isRed(h.left) && h.left != null && isRed(h.left.left))
            h = rotateRight(h);
        if (isRed(h.left) && isRed(h.right))
            flipColors(h);

        h.count = size(h.left) + size(h.right) + 1;
        return h;
    }

    private boolean isRed(Node node) {
        return node != null && node.isRed();
    }

    @Override
    public Iterator<Key> iterator() {
        Queue<Key> q = new LinkedListQueue<Key>();
        inOrderEnqueue(root, q);
        return q.iterator();
    }

    private void inOrderEnqueue(Node node, Queue<Key> queue) {
        if (node != null) {
            inOrderEnqueue(node.left, queue);
            queue.enqueue(node.key);
            inOrderEnqueue(node.right, queue);
        }
    }

    private Node rotateLeft(Node node) {
        Node aux = node;
        node = aux.right;
        aux.right = node.left;
        node.left = aux;
        node.color = aux.color;
        node.count = aux.count;
        aux.color = Color.RED;
        aux.count = 1 + size(aux.left) + size(aux.right);
        return node;
    }

    private int size(Node node) {
        if (node == null) return 0;
        return node.count;
    }

    private Node rotateRight(Node node) {
        Node aux = node;
        node = aux.left;
        aux.left = node.right;
        node.right = aux;
        node.color = aux.color;
        node.count = aux.count;
        aux.color = Color.RED;
        aux.count = 1 + size(aux.left) + size(aux.right);
        return node;
    }

    private void flipColors(Node node) {
        if (node != null) {
            node.color = Color.RED;
            if (node.left != null)
                node.left.color = Color.BLACK;
            if (node.right != null)
                node.right.color = Color.BLACK;
        }
    }

    private class Node {
        Key key;
        Val val;
        Node left, right;
        int count;
        Color color = Color.RED;

        Node(Key key, Val val) {
            this.key = key;
            this.val = val;
        }

        public boolean isRed() {
            return Color.RED.equals(color);
        }
    }

    private enum Color {
        BLACK, RED
    }
}
