package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;
import org.bitbucket.frico.ds.SymbolTable;

import java.util.Iterator;

/**
 * Created by feliperico on 04/11/15.
 */
public class BSTSymbolTable<Key extends Comparable<Key>, Val> implements SymbolTable<Key, Val>{

    private Node root;

    @Override
    public void put(Key key, Val value) {
        if (key != null && value != null) {
            root = put(root, key, value);
        }
    }

    private Node put(Node node, Key key, Val val) {
        if (node == null) {
            node = new Node(key, val);
        } else {
            int comp = key.compareTo(node.key);
            if (comp == 0) {
                node.val = val;
            } else if (comp < 0) {
                node.left = put(node.left, key, val);
            } else { /* comp > 0 */
                node.right = put(node.right, key, val);
            }
        }
        node.count = 1 + size(node.left) + size(node.right);
        return node;
    }

    @Override
    public void delete(Key key) {
        root = delete(root, key);
    }

    private Node delete(Node node, Key key) {
        if (node != null) {
            int comp = key.compareTo(node.key);
            if (comp < 0) {
                node.left = delete(node.left, key);
            } else if (comp > 0) {
                node.right = delete(node.right, key);
            } else {
                if (node.right == null) return node.left;
                if (node.left == null) return node.right;

                Node aux = node;
                node = min(aux.right);
                node.right = deleteMin(aux.right);
                node.left = aux.left;

            }

            node.count = 1 + size(node.left) + size(node.right);
            return node;
        }
        return null;
    }

    private Node min(Node node) {
        if (node != null && node.left != null) {
            return min(node.left);
        }
        return node;
    }

    private Node deleteMin(Node node) {
        if (node != null) {
            if (node.left == null) {
                return node.right;
            } else {
                node.left = deleteMin(node.left);
                node.count = 1 + size(node.left) + size(node.right);
            }
        }
        return node;
    }

    @Override
    public boolean contains(Key key) {
        return search(key) != null;
    }

    @Override
    public Val get(Key key) {
        return search(key);
    }

    private Val search(Key key) {
        if (root != null && key != null) {
            Node curr = root;
            while (curr != null) {
                if (key.compareTo(curr.key) == 0) {
                    return curr.val;
                } else if (key.compareTo(curr.key) < 0) {
                    curr = curr.left;
                } else { /* key.compareTo(curr.key) > 0 */
                    curr = curr.right;
                }
            }
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node node) {
        if (node == null) return 0;
        return node.count;
    }

    @Override
    public Iterator<Key> iterator() {
        Queue<Key> q = new LinkedListQueue<Key>();
        inOrderEnqueue(root, q);
        return q.iterator();
    }

    private void inOrderEnqueue(Node node, Queue<Key> queue) {
        if (node != null) {
            inOrderEnqueue(node.left, queue);
            queue.enqueue(node.key);
            inOrderEnqueue(node.right, queue);
        }
    }

    private class Node {
        Key key;
        Val val;
        Node left, right;
        int count;

        Node(Key key, Val val) {
            this.key = key;
            this.val = val;
        }

    }
}
