package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Stack;

import java.util.Iterator;

/**
 * Created by feliperico on 21/10/15.
 */
public class LinkedListStack<Item> implements Stack<Item> {

    private Node top;
    private int currSize = 0;

    @Override
    public void push(Item item) {
        Node oldTop = top;
        top = new Node();
        top.item = item;
        top.next = oldTop;
        currSize++;
    }

    @Override
    public Item pop() {
        Node oldTop = top;
        if (top != null) {
            top = top.next;
            currSize--;
            return oldTop.item;
        }
        return null;
    }

    @Override
    public Item peek() {
        if (top != null) {
            return top.item;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

    @Override
    public int size() {
        return currSize;
    }

    @Override
    public Iterator<Item> iterator() {
        return new LLStackIterator();
    }

    private class Node {
        Item item;
        Node next;
    }

    private class LLStackIterator implements Iterator<Item> {

        Node current = top;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (hasNext()) {
                Node nextNode = current;
                current = current.next;
                return nextNode.item;
            }
            return null;
        }

        @Override
        public void remove() {
        }
    }
}
