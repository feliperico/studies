package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.SymbolTable;

import java.util.Iterator;

/**
 * Created by feliperico on 12/01/16.
 */
@SuppressWarnings("unchecked")
public class HashChainSymbolTable<Key extends Comparable<Key>, Val> implements SymbolTable<Key, Val> {
    private static final int DEFAUT_SIZE = 97;

    private class Node<Key extends Comparable<Key>, Val> {
        int hash;
        Key key;
        Val val;
        Node next;
    }

    private Node[] nodes;
    private int currSize;

    public HashChainSymbolTable() {
        nodes = new Node[DEFAUT_SIZE];
    }

    @Override
    public void put(Key key, Val value) {
        if (key == null) throw new IllegalArgumentException();

        int hash = hash(key);
        Node<Key, Val> node = getEqualOrLastOrNull(key);
        if (node != null) {
            if (node.key.equals(key)) {
                node.val = value;
            } else {
                node.next = createNode(hash, key, value);
            }
        } else {
            nodes[hash] = createNode(hash, key, value);
        }
    }

    private Node<Key, Val> createNode(int hash, Key key, Val val) {
        currSize++;
        Node<Key, Val> node = new Node<>();
        node.hash = hash;
        node.key = key;
        node.val = val;
        return node;
    }

    @Override
    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException();

        Node<Key, Val> node = getEqualOrLastOrNull(key);
        return (node != null) && node.key.equals(key);

    }

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % DEFAUT_SIZE;
    }

    @Override
    public Val get(Key key) {
        if (key != null) {
            Node<Key, Val> node = getEqualOrLastOrNull(key);
            if (node != null && node.key.equals(key)) {
                return node.val;
            }
        }
        return null;
    }

    private Node<Key, Val> getEqualOrLastOrNull(Key key) {
        int hash = hash(key);
        Node<Key, Val> node = nodes[hash];
        if (node != null) {
            while (!node.key.equals(key) && node.next != null) {
                node = node.next;
            }
        }
        return node;
    }

    @Override
    public boolean isEmpty() {
        return currSize == 0;
    }

    @Override
    public int size() {
        return currSize;
    }

    @Override
    public void delete(Key key) {
        if (key != null) {
            int hash = hash(key);
            if (nodes[hash] != null) {
                if (nodes[hash].key.equals(key)) {
                    nodes[hash] = nodes[hash].next;
                } else {
                    Node<Key, Val> node = nodes[hash];
                    while (node.next != null && !node.next.key.equals(key)) {
                        node = node.next;
                    }
                    if (node.next != null) {
                        node.next = node.next.next;
                    }
                }
                currSize--;
            }
        }
    }

    @Override
    public Iterator<Key> iterator() {
        return new HashChainIterator();
    }

    private class HashChainIterator implements Iterator<Key> {

        Key[] iterKeys;
        int iterKeyIndex = -1;

        HashChainIterator() {
            if (currSize > 0) {
                iterKeys = (Key[]) new Comparable[currSize];
                int iterIdx = 0;
                for (int i = 0; i < nodes.length; i++) {
                    if (nodes[i] != null) {
                        Node<Key, Val> curr = nodes[i];
                        do {
                            iterKeys[iterIdx++] = curr.key;
                            curr = curr.next;
                        } while (curr != null);
                    }
                }
                iterKeyIndex = 0;
            }
        }

        @Override
        public boolean hasNext() {
            return iterKeyIndex >= 0 && iterKeyIndex < currSize;
        }

        @Override
        public Key next() {
            if (hasNext()) {
                return iterKeys[iterKeyIndex++];
            }
            return null;
        }

        @Override
        public void remove() {

        }
    }
}
