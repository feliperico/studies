package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Stack;

import java.util.Iterator;

/**
 * Created by feliperico on 21/10/15.
 */
public class ArrayStack<Item> implements Stack<Item> {
    private static final int INITIAL_CAPACITY = 10;

    private int topIndex = -1;
    private Item[] items;

    @SuppressWarnings("unchecked")
    public ArrayStack() {
        items = (Item[]) new Object[INITIAL_CAPACITY];
    }

    @Override
    public void push(Item item) {
        if (item != null) {
            if (topIndex == items.length - 1) {
                resizeArray(items.length * 2);
            }
            items[++topIndex] = item;
        } else {
            throw new IllegalArgumentException("Item cannot be null");
        }
    }

    @SuppressWarnings("unchecked")
    private void resizeArray(int newCapacity) {
        Item[] newItems = (Item[]) new Object[newCapacity];
        int limit = newCapacity > items.length ? items.length - 1 : newCapacity - 1;
        System.arraycopy(items, 0, newItems, 0, limit + 1);
        items = newItems;
    }

    @Override
    public Item pop() {
        if (topIndex > -1) {
            if (topIndex < (items.length / 4)) {
                resizeArray(items.length / 2);
            }
            Item ret = items[topIndex];
            items[topIndex--] = null;
            return ret;
        }
        return null;
    }

    @Override
    public Item peek() {
        if (topIndex > -1) {
            return items[topIndex];
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return topIndex == -1;
    }

    @Override
    public int size() {
        return topIndex + 1;
    }

    @Override
    public Iterator<Item> iterator() {
        return new AStackIterator();
    }

    private class AStackIterator implements Iterator<Item> {

        private int currIndex = topIndex;

        @Override
        public boolean hasNext() {
            return currIndex > -1;
        }

        @Override
        public Item next() {
            if (hasNext()) {
                return items[currIndex--];
            }
            return null;
        }

        @Override
        public void remove() {

        }
    }
}
