package org.bitbucket.frico.ds;

/**
 * Directed graph
 *
 * Created by feliperico on 02/04/16.
 */
public class Digraph extends Graph {

    public Digraph(int numberOfVertices) {
        super(numberOfVertices);
    }

    public void addEdge(int v, int w) {
        if (v >= 0 && w  >=0 && v < numberOfVertices && w < numberOfVertices) {
            vertices[v].add(w);
        }
    }

    public Digraph reverse() {
        Digraph reversed = new Digraph(numberOfVertices);
        for (int v = 0; v < numberOfVertices; v++) {
            for (int w : this.adjacent(v)) {
                reversed.addEdge(w, v);
            }
        }
        return reversed;
    }
}
