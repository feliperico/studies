package org.bitbucket.frico.ds;

/**
 * Created by feliperico on 22/10/15.
 */
public interface Queue<Item> extends Iterable<Item> {

    void enqueue(Item item);

    Item dequeue();

    Item first();

    boolean isEmpty();

    int size();
}
