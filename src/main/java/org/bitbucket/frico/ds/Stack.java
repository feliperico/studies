package org.bitbucket.frico.ds;

/**
 * Created by feliperico on 21/10/15.
 */
public interface Stack<Item> extends Iterable<Item> {

    void push(Item item);

    Item pop();

    Item peek();

    boolean isEmpty();

    int size();
}
