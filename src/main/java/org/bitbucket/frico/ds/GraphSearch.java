package org.bitbucket.frico.ds;

/**
 * Graph search interface
 * Created by feliperico on 18/02/16.
 */
public interface GraphSearch {

    boolean isConnected(int vertex);

    int totalConnections();
}
