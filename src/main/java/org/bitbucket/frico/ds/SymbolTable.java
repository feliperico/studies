package org.bitbucket.frico.ds;

/**
 * Created by feliperico on 29/10/15.
 */
public interface SymbolTable<Key extends Comparable<Key>, Val> extends Iterable<Key> {

    void put(Key key, Val value);

    boolean contains(Key key);

    Val get(Key key);

    boolean isEmpty();

    int size();

    void delete(Key key);

}
