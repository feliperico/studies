package org.bitbucket.frico.ds;

import org.bitbucket.frico.algs.SortingUtils;

/**
 * Created by feliperico on 29/10/15.
 */
public class MaxPQ<Key extends Comparable<Key>> extends SortingUtils {

    private Key[] array;
    int size = 0;

    @SuppressWarnings("unchecked")
    public MaxPQ(int capacity) {
        array = (Key[]) new Comparable[capacity + 1];
    }

    public void insert(Key key) {
        if (size < array.length) {
            array[++size] = key;
            swin(size);
        }
    }

    public Key delMax() {
        if (size > 0) {
            Key key = array[1];
            swap(1, size--, array);
            array[size + 1] = null;
            sink(1);
            return key;
        }
        return null;
    }

    private void sink(int i) {
        while (i*2 <= size && lessThanOneChild(i)) {
            int greater = i*2;
            if (array[greater + 1] != null && less(array[greater], array[greater + 1])) greater++;
            swap(i, greater, array);
            i = greater;
        }
    }

    private boolean lessThanOneChild(int i) {
        if (less(array[i], array[i * 2])) {
            return true;
        } else if (array[(i * 2) + 1] != null && less(array[i], array[(i * 2) + 1])) {
            return true;
        }
        return false;
    }

    private void swin(int i) {
        while (i > 1 && less(array[i/2], array[i])) {
            swap(i/2, i, array);
            i /= 2;
        }
    }
}
