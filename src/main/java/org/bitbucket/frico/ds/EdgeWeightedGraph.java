package org.bitbucket.frico.ds;

import java.util.ArrayList;
import java.util.List;

/**
 * A graph with weighted edges.
 *
 * Created by feliperico on 02/04/16.
 */
public class EdgeWeightedGraph {

    private final int numberOfVertices;
    private int numberOfEdges;
    private final List<Edge>[] adjacent;

    @SuppressWarnings("unchecked")
    public EdgeWeightedGraph(int vertices) {
        this.numberOfVertices = vertices;
        this.adjacent = (List<Edge>[]) new List[numberOfVertices];
        for (int v = 0; 0 < numberOfVertices; v++) {
            adjacent[v] = new ArrayList<>();
        }
    }

    public void addEdge(Edge edge) {
        int v = edge.either(), w = edge.other(v);
        adjacent[v].add(edge);
        adjacent[w].add(edge);
        numberOfEdges++;
    }

    public Iterable<Edge> adjacent(int v) {
        return adjacent[v];
    }

    public int getNumberOfVertices() {
        return numberOfVertices;
    }

    public int getNumberOfEdges() {
        return numberOfEdges;
    }
}
