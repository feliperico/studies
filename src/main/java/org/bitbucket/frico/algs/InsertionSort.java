package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 22/10/15.
 */
public class InsertionSort extends SortingUtils {

    public static void sort(Comparable[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j >= 1; j--) {
                if (less(array[j], array[j - 1])) {
                    swap(j, j - 1, array);
                } else {
                    break;
                }
            }
        }
    }

    public static void sort(Object[] array, Comparator comparator) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j >= 1; j--) {
                if (less(comparator, array[j], array[j - 1])) {
                    swap(j, j - 1, array);
                } else {
                    break;
                }
            }
        }
    }
}
