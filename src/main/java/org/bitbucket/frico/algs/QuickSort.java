package org.bitbucket.frico.algs;

/**
 * Created by feliperico on 27/10/15.
 */
public class QuickSort extends SortingUtils {

    public static void sort(Comparable[] array) {
        Random.shuffle(array);
        sort(array, 0, array.length - 1);
    }


    private static void sort(Comparable[] array, int lo, int hi) {
        if (lo < hi) {
            int medium = getMediumOfThree(array, lo, hi);
            swap(lo, medium, array);
            int mid = getMiddleIndex(array, lo, hi);
            sort(array, lo, mid - 1);
            sort(array, mid + 1, hi);
        }
    }

    private static int getMiddleIndex(Comparable[] array, int lo, int hi) {
        int k = lo;
        int i = k + 1, j = hi;
        while(true) {
            while (i < hi && less(array[i], array[k])) i++;
            while (j > lo && less(array[k], array[j])) j--;
            if (j <= i) break;
            swap(i, j, array);
        }
        swap(j, k, array);

        return j;
    }

    private static int getMediumOfThree(Comparable[] array, int lo, int hi) {
        int mid = lo + ((hi - lo) / 2);

        if (lessOrEqual(array[lo], array[hi]) && lessOrEqual(array[hi], array[mid])) {
            return hi;
        } else if (lessOrEqual(array[mid], array[lo]) && lessOrEqual(array[lo], array[hi])) {
            return lo;
        }

        return mid;
    }


    public static Comparable selectKthElement(Comparable[] array, int k) {
        Random.shuffle(array);
        int lo = 0, hi = array.length - 1;
        while (lo < hi) {
            int mid = getMiddleIndex(array, lo, hi);
            if (k > mid) {
                lo = mid + 1;
            } else if (k < mid) {
                hi = mid - 1;
            } else {
                return array[k];
            }
        }

        return array[k];
    }
}
