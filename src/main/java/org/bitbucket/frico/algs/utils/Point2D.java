package org.bitbucket.frico.algs.utils;

import java.util.Comparator;

/**
 * Created by feliperico on 23/10/15.
 */
public class Point2D {
    public static final Comparator<Point2D> Y_ORDER_COMPARATOR = new Comparator<Point2D>() {
        @Override
        public int compare(Point2D o1, Point2D o2) {
            if (o1 != null && o2 != null) {
                if (o1.getYCoord() < o2.getYCoord()) {
                    return -1;
                } else if (o1.getYCoord() == o2.getYCoord()) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                throw new IllegalArgumentException();
            }
        }
    };

    private double xCoord;
    private double yCoord;

    public Point2D(double xCoord, double yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public double getXCoord() {
        return xCoord;
    }

    public void setXCoord(double xCoord) {
        this.xCoord = xCoord;
    }

    public double getYCoord() {
        return yCoord;
    }

    public void setYCoord(double yCoord) {
        this.yCoord = yCoord;
    }

    public Comparator<Point2D> getPolarOrderComparator() {
        return new Comparator<Point2D>() {
            @Override
            public int compare(Point2D o1, Point2D o2) {
                double dx1 = o1.getXCoord() - xCoord;
                double dy1 = o1.getYCoord() - yCoord;
                double dx2 = o2.getXCoord() - xCoord;
                double dy2 = o2.getYCoord() - yCoord;

                if      (dy1 >= 0 && dy2 < 0) return -1;    // o1 above; o2 below
                else if (dy2 >= 0 && dy1 < 0) return +1;    // o1 below; o2 above
                else if (dy1 == 0 && dy2 == 0) {            // 3-collinear and horizontal
                    if      (dx1 >= 0 && dx2 < 0) return -1;
                    else if (dx2 >= 0 && dx1 < 0) return +1;
                    else                          return  0;
                }
                // Note: ccw() recomputes dx1, dy1, dx2, and dy2
                else return -ccw(Point2D.this, o1, o2);     // both above or below
            }
        };
    }

    /**
     * Returns true if a->b->c is a counterclockwise turn.
     * @param a first point
     * @param b second point
     * @param c third point
     * @return { -1, 0, +1 } if a->b->c is a { clockwise, collinear; counterclocwise } turn.
     */
    public static int ccw(Point2D a, Point2D b, Point2D c) {
        double area2 = (b.getXCoord()-a.getXCoord())*(c.getYCoord()-a.getYCoord()) - (b.getYCoord()-a.getYCoord())*(c.getXCoord()-a.getXCoord());
        if      (area2 < 0) return -1;
        else if (area2 > 0) return +1;
        else                return  0;
    }
}
