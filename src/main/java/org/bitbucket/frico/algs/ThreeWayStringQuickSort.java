package org.bitbucket.frico.algs;

public class ThreeWayStringQuickSort extends SortingUtils {

    public static String[] sort(String[] strings) {
        sort(strings, 0, strings.length - 1, 0);
        return strings;
    }

    private static void sort(String[] strings, int lo, int hi, int d) {
        if (hi <= lo) {
            return;
        }

        int lt = lo, gt = hi;
        int v = charAt(strings[lo], d);
        int i = lo + 1;

        while (i <= gt) {
            int t = charAt(strings[i], d);
            if (t < v) {
                swap(lt++, i++, strings);
            } else if (t > v) {
                swap(i, gt--, strings);
            } else {
                i++;
            }
        }

        sort(strings, lo, lt - 1, d);
        if (v >= 0) {
            sort(strings, lt, gt, d + 1);
        }
        sort(strings, gt + 1, hi, d);

    }

    /**
     * Consider -1 if index is greater than the
     * maximum index in the string
     * @param string
     * @param index
     * @return char at index or -1 if index is greater than
     * the allowed value
     */
    private static int charAt(String string, int index) {
        if (index < string.length()) {
            return string.charAt(index);
        }
        return -1;
    }

}
