package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 26/10/15.
 */
public class BottomUpMergeSort extends MergeSort {


    public static void sort(Comparable[] array) {
        aux1 = new Comparable[array.length];
        for (int sz = 1; sz < array.length; sz = sz + sz) {
            for (int lo = 0; lo < array.length - sz; lo += 2 * sz) {
                int mid = lo + sz - 1;
                merge(array, lo, mid, Math.min(lo + (2 * sz) - 1, array.length - 1));
            }
        }
        aux1 = null;
    }

    public static void sort(Object[] array, Comparator comparator) {
        aux2 = new Object[array.length];
        for (int sz = 1; sz < array.length; sz = sz + sz) {
            for (int lo = 0; lo < array.length - sz; lo += 2 * sz) {
                int mid = lo + sz - 1;
                merge(array, comparator, lo, mid, Math.min(lo + (2 * sz) - 1, array.length - 1));
            }
        }
        aux2 = null;
    }

}
