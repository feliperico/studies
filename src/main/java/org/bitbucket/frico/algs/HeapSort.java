package org.bitbucket.frico.algs;

/**
 * Created by feliperico on 29/10/15.
 */
public class HeapSort extends SortingUtils{

    public static void sort(Comparable[] array) {
        int size = array.length;
        for (int k = size/2; k >= 1; k--) {
            sink(array, k, size);
        }
        while (size > 1) {
            swap(getIndex(1), getIndex(size), array);
            sink(array, 1, --size);
        }

    }

    private static void sink(Comparable[] array, int i, int size) {
        while(2*i <= size && lessThanOneChild(array, i, size)) {
            int greater = 2*i;
            if (greater + 1 <= size &&
                    less(array[getIndex(greater)], array[getIndex(greater + 1)])) {
                greater++;
            }
            swap(getIndex(i), getIndex(greater), array);
            i = greater;
        }
    }

    private static boolean lessThanOneChild(Comparable[] array, int i, int size) {
        if (less(array[getIndex(i)], array[getIndex(2*i)])) {
            return true;
        } else if ((2*i) + 1 <= size && less(array[getIndex(i)], array[getIndex((2*i) + 1)])) {
            return true;
        }
        return false;
    }

    private static int getIndex(int i) {
        return i - 1;
    }
}
