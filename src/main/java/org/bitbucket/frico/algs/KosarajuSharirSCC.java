package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Digraph;

/**
 * Created by feliperico on 02/04/16.
 */
public class KosarajuSharirSCC {

    private int[] id;
    private boolean[] marked;
    private int count;

    public KosarajuSharirSCC(Digraph digraph) {
        id = new int[digraph.numberOfVertices()];
        marked = new boolean[digraph.numberOfVertices()];
        DepthFirstOrder dfo = new DepthFirstOrder(digraph.reverse());
        for (int v : dfo.getReversePost()) {
            if (!marked[v]) {
                dfs(digraph, v);
                count++;
            }
        }
    }

    private void dfs(Digraph digraph, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : digraph.adjacent(v)) {
            if (!marked[w]) {
                dfs(digraph, w);
            }
        }
    }

    public boolean isStronglyConnected(int v, int w) {
        return id[v] == id[w];
    }
}
