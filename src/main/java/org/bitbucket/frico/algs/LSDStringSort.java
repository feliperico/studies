package org.bitbucket.frico.algs;

public class LSDStringSort {

    public static String[] caseSensitiveSort(String[] strings, int wordSize) {
        int radix = 256;
        int n = strings.length;
        String[] aux = new String[n];

        for (int d = wordSize - 1; d >= 0; d--) {
            int[] count = new int[radix + 1];

            // 1 - count frequencies of each character
            for (int i = 0; i < n; i++) {
                count[strings[i].charAt(d) + 1]++;
            }

            // 2 - compute cumulates
            for (int i = 0; i < radix; i++) {
                count[i + 1] += count[i];
            }

            // 3 - move items to their right position in the auxiliary array
            for (int i = 0; i < n; i++) {
                aux[count[strings[i].charAt(d)]++] = strings[i];
            }

            // 4 - copy sorted auxiliary back to the original strings array
            for (int i = 0; i < n; i++) {
                strings[i] = aux[i];
            }
        }

        return strings;
    }
}
