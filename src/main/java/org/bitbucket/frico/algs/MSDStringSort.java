package org.bitbucket.frico.algs;

public class MSDStringSort {

    public static String[] sort(String[] strings) {
        // 1 - use key indexed counting to partition the array
        // into R pieces.
        // 2 - recursively sort the strings that start with each character
        // (key indexed sorting delineates each array to sort)
        String[] aux = new String[strings.length];
        sort(strings, aux, 0, strings.length - 1, 0);
        return strings;
    }

    private static void sort(String[] strings, String[] aux, int lo, int hi, int d) {
        if (hi <= lo) {
            return;
        }
        int R = 256;
        int[] count = new int[R + 2];
        // 1 - count frequencies
        for (int i = lo; i <= hi; i++) {
            count[charAt(strings[i], d) + 2]++;
        }

        // 2 - compute cumulates
        for (int i = 0; i < R + 1; i++) {
            count[i + 1] += count[i];
        }

        // 3 - place strings in the right position in aux
        for (int i = lo; i <= hi; i++) {
            aux[count[charAt(strings[i], d) + 1]++] = strings[i];
        }

        // 4 - copy to the input array
        for (int i = lo; i <= hi; i++) {
            strings[i] = aux[i - lo];
        }

        // recursively sorts subarrays
        for (int i = 0; i < R; i ++) {
            sort(strings, aux, lo + count[i], lo + count[i+1] - 1, d+1);
        }
    }


    /**
     * Consider -1 if index is greater than the
     * maximum index in the string
     * @param string
     * @param index
     * @return char at index or -1 if index is greater than
     * the allowed value
     */
    private static int charAt(String string, int index) {
        if (index < string.length()) {
            return string.charAt(index);
        }
        return -1;
    }

}
