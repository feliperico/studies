package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Graph;

/**
 * Computes the connected components of a graph.
 *
 * Created by feliperico on 07/03/16.
 */
public class ConnectedComponents {

    private boolean[] marked;
    private int[] ids;
    private int counter;

    public ConnectedComponents(Graph g) {
        marked = new boolean[g.numberOfVertices()];
        ids = new int[g.numberOfVertices()];
        for (int v = 0; v < g.numberOfVertices(); v++) {
            if (!marked[v]) {
                dfs(v, g);
                counter++;
            }
        }
    }

    private void dfs(int v, Graph g) {
        marked[v] = true;
        ids[v] = counter;
        for (int w : g.adjacent(v)) {
            if (!marked[w]) {
                dfs(v, g);
            }
        }
    }

    public int size() {
        return counter;
    }

    public int id(int v) {
        return ids[v];
    }

    public boolean isConnected(int v, int w) {
        return id(v) == id(w);
    }
}
