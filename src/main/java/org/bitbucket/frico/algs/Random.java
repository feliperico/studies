package org.bitbucket.frico.algs;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by feliperico on 23/10/15.
 */
public class Random {

    public static void shuffle(Object[] array) {
        for (int i = 1; i < array.length; i++) {
            int rnd = ThreadLocalRandom.current().nextInt(0, i);
            SortingUtils.swap(i, rnd, array);
        }
    }

    public static void main(String[] args) {
        Object[] array = new Object[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55, 66};
        shuffle(array);
        System.out.println(Arrays.toString(array));
    }
}
