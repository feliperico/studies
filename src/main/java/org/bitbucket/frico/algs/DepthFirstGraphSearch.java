package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Graph;
import org.bitbucket.frico.ds.GraphSearch;

/**
 * Depth First Search
 *
 * Created by feliperico on 18/02/16.
 */
public class DepthFirstGraphSearch implements GraphSearch {

    private boolean[] marked;
    private int connectionsCounter;

    public DepthFirstGraphSearch(Graph graph, int v) {
        marked = new boolean[graph.numberOfVertices()];
        dfs(graph, v);
    }

    private void dfs(Graph graph, int v) {
        marked[v] = true;
        connectionsCounter++;
        for (Integer w : graph.adjacent(v)) {
            if (!marked[w]) dfs(graph, w);
        }
    }

    @Override
    public boolean isConnected(int vertex) {
        return marked[vertex];
    }

    @Override
    public int totalConnections() {
        return connectionsCounter;
    }
}
