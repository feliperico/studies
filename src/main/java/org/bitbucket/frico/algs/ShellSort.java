package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 23/10/15.
 */
public class ShellSort extends SortingUtils {

    public static void sort(Comparable[] array) {
        int h = 1;
        while (h < array.length / 3) h = h * 3 + 1;

        while (h >= 1) {
            for (int i = h; i < array.length; i++) {
                for (int j = i; j >= h; j -= h) {
                    if (less(array[j], array[j - h])) {
                        swap(j, j - h, array);
                    }
                }
            }
            h = h / 3;
        }
    }

    public static void sort(Object[] array, Comparator comparator) {
        int h = 1;
        while (h < array.length / 3) h = h * 3 + 1;

        while (h >= 1) {
            for (int i = h; i < array.length; i++) {
                for (int j = i; j >= h; j -= h) {
                    if (less(comparator, array[j], array[j - h])) {
                        swap(j, j - h, array);
                    }
                }
            }
            h = h / 3;
        }
    }
}
