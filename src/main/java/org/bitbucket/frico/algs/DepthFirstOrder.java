package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Digraph;
import org.bitbucket.frico.ds.Stack;
import org.bitbucket.frico.ds.impl.LinkedListStack;

/**
 * This class does topological sort of a directed acyclic graph
 * by using depth first search with a reverse post order stack.
 *
 * Created by feliperico on 02/04/16.
 */
public class DepthFirstOrder {

    private boolean[] marked;
    private Stack<Integer> reversePost;

    public DepthFirstOrder(Digraph digraph) {
        marked = new boolean[digraph.numberOfVertices()];
        reversePost = new LinkedListStack<>();
        for (int v = 0; v < digraph.numberOfVertices(); v++) {
            if (!marked[v]) {
                dfs(digraph, v);
            }
        }
    }

    private void dfs(Digraph digraph, int v) {
        marked[v] = true;
        for (int w : digraph.adjacent(v)) {
            if (!marked[w]) {
                dfs(digraph, w);
            }
        }
        reversePost.push(v);
    }

    public Iterable<Integer> getReversePost() {
        return reversePost;
    }
}
