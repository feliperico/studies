package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Digraph;

/**
 * Breadth First Search for Digraphs
 *
 * Created by feliperico on 02/04/16.
 */
public class DirectedBFS extends BreadthFirstGraphSearch {

    public DirectedBFS(Digraph g, int source) {
        super(g, source);
    }
}
