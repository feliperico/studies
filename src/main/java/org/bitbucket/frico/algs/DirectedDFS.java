package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Digraph;

/**
 * Depth First Search for Digraphs
 *
 * Created by feliperico on 02/04/16.
 */
public class DirectedDFS extends DepthFirstGraphSearch {

    public DirectedDFS(Digraph graph, int v) {
        super(graph, v);
    }
}
