package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 27/10/15.
 */
public class ThreeWayQuickSort extends SortingUtils {


    public static void sort(Comparable[] array) {
        sort(array, 0, array.length - 1);
    }

    public static void sort(Object[] array, Comparator comparator) {
        sort(array, 0, array.length - 1, comparator);
    }

    static void sort(Object[] array, int lo, int hi, Comparator comparator) {
        if (lo < hi) {
            int le = lo, gt = hi, i = le + 1;
            Object pivot = array[le];
            while (gt >= i) {
                if (less(comparator, array[i], pivot)) {
                    swap(i++, le++, array);
                } else if (less(comparator, pivot, array[i])) {
                    swap(gt--, i, array);
                } else {
                    // a[i] == pivot
                    i++;
                }
            }

            sort(array, lo, le - 1, comparator);
            sort(array, gt + 1, hi, comparator);
        }
    }

    static void sort(Comparable[] array, int lo, int hi) {
        if (lo < hi) {
            int le = lo, gt = hi, i = le + 1;
            Comparable pivot = array[le];
            while (gt >= i) {
                if (less(array[i], pivot)) {
                    swap(i++, le++, array);
                } else if (less(pivot, array[i])) {
                    swap(gt--, i, array);
                } else {
                    // a[i] == pivot
                    i++;
                }
            }

            sort(array, lo, le - 1);
            sort(array, gt + 1, hi);
        }
    }
}
