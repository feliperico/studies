package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Digraph;
import org.bitbucket.frico.ds.Stack;
import org.bitbucket.frico.ds.impl.LinkedListStack;

/**
 * Searches a directed graph to check for the presence of
 * a cycle.
 *
 * Created by feliperico on 02/04/16.
 */
public class DirectedCycle {

    private boolean[] marked;
    private boolean[] onStack;
    private int[] edgeTo;
    private Stack<Integer> cycle;

    public DirectedCycle(Digraph digraph) {
        marked = new boolean[digraph.numberOfVertices()];
        onStack = new boolean[digraph.numberOfVertices()];
        edgeTo = new int[digraph.numberOfVertices()];
        for (int v = 0; v < digraph.numberOfVertices(); v++) {
            if (!marked[v]) {
                dfs(digraph, v);
            }
        }
    }

    private void dfs(Digraph digraph, int v) {
        marked[v] = true;
        onStack[v] = true;
        for (int w : digraph.adjacent(v)) {
            if (hasCycle()) {
                return;
            } else if (!marked[w]) {
                edgeTo[w] = v;
                dfs(digraph, w);
            } else if (onStack[w]) {
                cycle = new LinkedListStack<>();
                for (int x = v; x != w; x = edgeTo[x]) {
                    cycle.push(x);
                }
                cycle.push(w);
                cycle.push(v);
            }
        }
        onStack[v] = false;
    }

    public boolean hasCycle() {
        return cycle != null;
    }

    public Iterable<Integer> getCycle() {
        return cycle;
    }
}
