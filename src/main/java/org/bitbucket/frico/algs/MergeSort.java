package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 26/10/15.
 */
public class MergeSort extends SortingUtils {
    private static final int ARRAY_OFFSET = 10;

    protected static Comparable[] aux1;
    protected static Object[] aux2;

    public static void sort(Object[] array, Comparator comparator) {
        if (array.length < ARRAY_OFFSET) {
            InsertionSort.sort(array, comparator);
        } else {
            aux2 = new Object[array.length];
            sort(array, comparator, 0, array.length - 1);
            aux2 = null;
        }
    }

    private static void sort(Object[] array, Comparator comparator, int lo, int hi) {
        if ((hi - lo + 1) > 1) {
            int mid = ((hi - lo) / 2) + lo;
            sort(array, comparator, lo, mid);
            sort(array, comparator, mid + 1, hi);
            if (!less(comparator, array[mid], array[mid + 1])) {
                merge(array, comparator, lo, mid, hi);
            }
        }
    }

    protected static void merge(Object[] array, Comparator comparator, int lo, int mid, int hi) {
        int k = lo;
        int j = mid + 1;
        for (int i = lo; i <= hi; i++) {
            if (k > mid) {
                aux2[i] = array[j++];
            } else if (j > hi) {
                aux2[i] = array[k++];
            } else if (less(comparator, array[j], array[k])) {
                aux2[i] = array[j++];
            } else {
                aux2[i] = array[k++];
            }
        }

        for (int i = lo; i <= hi; i++) {
            array[i] = aux2[i];
        }
    }

    public static void sort(Comparable[] array) {
        if (array.length < ARRAY_OFFSET) {
            InsertionSort.sort(array);
        } else {
            aux1 = new Comparable[array.length];
            sort(array, 0, array.length - 1);
            aux1 = null;
        }
    }

    private static void sort(Comparable[] array, int lo, int hi) {
        if ((hi - lo + 1) > 1) {
            int mid = ((hi - lo) / 2) + lo;
            sort(array, lo, mid);
            sort(array, mid + 1, hi);
            if (!less(array[mid], array[mid + 1])) {
                merge(array, lo, mid, hi);
            }
        }
    }

    protected static void merge(Comparable[] array, int lo, int mid, int hi) {
        int k = lo;
        int j = mid + 1;
        for (int i = lo; i <= hi; i++) {
            if (k > mid) {
                aux1[i] = array[j++];
            } else if (j > hi) {
                aux1[i] = array[k++];
            } else if (less(array[j], array[k])) {
                aux1[i] = array[j++];
            } else {
                aux1[i] = array[k++];
            }
        }

        for (int i = lo; i <= hi; i++) {
            array[i] = aux1[i];
        }
    }
}
