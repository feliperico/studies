package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 22/10/15.
 */
public class SelectionSort extends SortingUtils {

    public static void sort(Comparable[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = minIndex; j < array.length; j++) {
                if (less(array[j], array[minIndex])) {
                    minIndex = j;
                }
            }
            swap(minIndex, i, array);
        }
    }

    public static void sort(Object[] array, Comparator comparator) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = minIndex; j < array.length; j++) {
                if (less(comparator, array[j], array[minIndex])) {
                    minIndex = j;
                }
            }
            swap(minIndex, i, array);
        }
    }
}
