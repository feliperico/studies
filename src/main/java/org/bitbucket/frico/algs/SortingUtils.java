package org.bitbucket.frico.algs;

import java.util.Comparator;

/**
 * Created by feliperico on 22/10/15.
 */
@SuppressWarnings("unchecked")
public class SortingUtils {

    public static boolean less(Comparable a, Comparable b) {
        return a.compareTo(b) < 0;
    }

    public static boolean less(Comparator c, Object a, Object b) {
        return c.compare(a, b) < 0;
    }

    public static void swap(int from, int to, Object[] array) {
        if (from != to) {
            Object dest = array[to];
            array[to] = array[from];
            array[from] = dest;
        }
    }

    public static boolean isSorted(Comparable[] array) {
        for (int i = 1; i < array.length; i++) {
            if (!lessOrEqual(array[i - 1], array[i])) return false;
        }
        return true;
    }

    public static boolean isSorted(Comparator comparator, Object[] array) {
        for (int i = 1; i < array.length; i++) {
            if (!lessOrEqual(comparator, array[i - 1], array[i])) return false;
        }
        return true;
    }

    public static boolean lessOrEqual(Comparable a, Comparable b) {
        return a.compareTo(b) < 0 || a.compareTo(b) == 0;
    }

    public static boolean lessOrEqual(Comparator c, Object a, Object b) {
        int comp = c.compare(a, b);
        return comp < 0 || comp == 0;
    }

    public static boolean binarySearch(Comparable[] array, Comparable target) {
        int lo = 0, hi = array.length - 1;
        int mid = lo + ((hi - lo + 1) / 2);

        while ((hi - lo + 1) >= 1) {
            if (array[mid].compareTo(target) == 0) {
                return true;
            } else if (less(array[mid], target)) {
                lo = mid + 1;
            } else {
                hi = mid - 1;
            }

            mid = lo + ((hi - lo + 1) / 2);
        }

        return false;
    }
}
