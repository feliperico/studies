package org.bitbucket.frico.algs;

import org.bitbucket.frico.ds.Graph;
import org.bitbucket.frico.ds.GraphSearch;
import org.bitbucket.frico.ds.Queue;
import org.bitbucket.frico.ds.impl.LinkedListQueue;

/**
 * BFS
 * Created by feliperico on 22/02/16.
 */
public class BreadthFirstGraphSearch implements GraphSearch {

    private boolean[] marked;
    private int[] edgeTo;
    private int connectionsCounter;

    public BreadthFirstGraphSearch(Graph g, int source) {
        marked = new boolean[g.numberOfVertices()];
        edgeTo = new int[g.numberOfVertices()];
        bfs(g, source);
    }

    private void bfs(Graph g, int source) {
        Queue<Integer> q = new LinkedListQueue<>();
        q.enqueue(source);
        marked[source] = true;
        while (!q.isEmpty()) {
            int v = q.dequeue();
            for (int w : g.adjacent(v)) {
                if (!marked[w]) {
                    q.enqueue(w);
                    marked[w] = true;
                    edgeTo[w] = v;
                    connectionsCounter++;
                }
            }
        }
    }

    @Override
    public boolean isConnected(int vertex) {
        return marked[vertex];
    }

    @Override
    public int totalConnections() {
        return connectionsCounter;
    }

    public int getEdgeTo(int v) {
        return edgeTo[v];
    }
}
