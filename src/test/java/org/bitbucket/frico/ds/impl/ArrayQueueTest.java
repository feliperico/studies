package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 22/10/15.
 */
public class ArrayQueueTest {

    @Test
    public void testSeveralOperations() {
        Queue<String> queue = new ArrayQueue<String>();
        Assert.assertTrue(queue.isEmpty());
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        Assert.assertFalse(queue.isEmpty());
        Assert.assertEquals(3, queue.size());
        Assert.assertEquals("1", queue.first());
        Assert.assertEquals(3, queue.size());
        Assert.assertEquals("1", queue.dequeue());
        Assert.assertEquals("2", queue.dequeue());
        Assert.assertEquals(1, queue.size());
        Assert.assertEquals("3", queue.dequeue());
        Assert.assertEquals(0, queue.size());
        Assert.assertNull(queue.dequeue());
        Assert.assertEquals(0, queue.size());
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        queue.enqueue("5");
        queue.enqueue("6");
        queue.enqueue("7");
        queue.enqueue("8");
        queue.enqueue("9");
        queue.enqueue("10");
        queue.enqueue("11");
        queue.enqueue("12");
        queue.enqueue("13");
        queue.enqueue("14");
        queue.enqueue("15");
        int iterations = 0;
        for (String item : queue) {
            iterations++;
        }
        Assert.assertEquals(15, iterations);
        iterations = 0;
        for (String item : queue) {
            iterations++;
        }
        Assert.assertEquals(15, iterations);
        Assert.assertEquals(15, queue.size());
        Assert.assertFalse(queue.isEmpty());
        Assert.assertEquals("1", queue.first());
        Assert.assertEquals("1", queue.dequeue());
        Assert.assertEquals("2", queue.dequeue());
        Assert.assertEquals("3", queue.dequeue());
        Assert.assertEquals("4", queue.dequeue());
        Assert.assertEquals("5", queue.dequeue());
        Assert.assertEquals("6", queue.dequeue());
        Assert.assertEquals("7", queue.dequeue());
        Assert.assertEquals("8", queue.dequeue());
        Assert.assertEquals("9", queue.dequeue());
        Assert.assertEquals("10", queue.dequeue());
        Assert.assertEquals("11", queue.dequeue());
        Assert.assertEquals("12", queue.dequeue());
        Assert.assertEquals(3, queue.size());
    }
}
