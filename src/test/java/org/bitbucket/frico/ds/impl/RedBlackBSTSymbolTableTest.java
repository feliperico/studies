package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.SymbolTable;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 09/11/15.
 */
public class RedBlackBSTSymbolTableTest {

    @Test
    public void testSymbolTableOperations() {
        SymbolTable<Integer, String> st = new RedBlackBSTSymbolTable<Integer, String>();
        Assert.assertTrue(st.isEmpty());
        st.put(5, "E");
        st.put(4, "D");
        st.put(1, "A");
        st.put(3, "C");
        st.put(2, "B");
        Assert.assertFalse(st.isEmpty());
        Assert.assertEquals(5, st.size());
        Assert.assertTrue(st.contains(1));
        Assert.assertTrue(st.contains(2));
        Assert.assertTrue(st.contains(3));
        Assert.assertTrue(st.contains(4));
        Assert.assertTrue(st.contains(5));
        Assert.assertEquals("A", st.get(1));
        Assert.assertEquals("B", st.get(2));
        Assert.assertEquals("C", st.get(3));
        Assert.assertEquals("D", st.get(4));
        Assert.assertEquals("E", st.get(5));

        int count = 0;
        for (Integer key : st) {
            count++;
        }
        Assert.assertEquals(count, st.size());
//        st.delete(3);
//        Assert.assertEquals(4, st.size());
//        st.delete(5);
//        Assert.assertEquals(3, st.size());
//        st.delete(2);
//        Assert.assertEquals(2, st.size());
//        st.delete(4);
//        Assert.assertEquals(1, st.size());
//        st.delete(1);
//        Assert.assertEquals(0, st.size());
//        Assert.assertTrue(st.isEmpty());
    }
}
