package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.MaxPQ;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 29/10/15.
 */
public class MaxPQTest {


    @Test
    public void testMaxPQ() {
        MaxPQ<Integer> pq = new MaxPQ<Integer>(10);
        pq.insert(1);
        pq.insert(2);
        pq.insert(3);
        Assert.assertEquals(new Integer(3), pq.delMax());
        Assert.assertEquals(new Integer(2), pq.delMax());
        Assert.assertEquals(new Integer(1), pq.delMax());
        pq.insert(12);
        pq.insert(245);
        pq.insert(33);
        pq.insert(21);
        pq.insert(32);
        pq.insert(553);
        pq.insert(100);
        pq.insert(20);
        pq.insert(33);
        Assert.assertEquals(new Integer(553), pq.delMax());
        Assert.assertEquals(new Integer(245), pq.delMax());
        Assert.assertEquals(new Integer(100), pq.delMax());
        Assert.assertEquals(new Integer(33), pq.delMax());
        Assert.assertEquals(new Integer(33), pq.delMax());
        Assert.assertEquals(new Integer(32), pq.delMax());
        Assert.assertEquals(new Integer(21), pq.delMax());
        Assert.assertEquals(new Integer(20), pq.delMax());
        Assert.assertEquals(new Integer(12), pq.delMax());
    }
}
