package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Stack;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 21/10/15.
 */
public class ArrayStackTest {

    @Test
    public void testSeveralOperations() {
        Stack<String> stack = new ArrayStack<String>();
        Assert.assertTrue(stack.isEmpty());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        Assert.assertFalse(stack.isEmpty());
        Assert.assertEquals(3, stack.size());
        Assert.assertEquals("3", stack.peek());
        Assert.assertEquals(3, stack.size());
        Assert.assertEquals("3", stack.pop());
        Assert.assertEquals("2", stack.pop());
        Assert.assertEquals(1, stack.size());
        Assert.assertEquals("1", stack.pop());
        Assert.assertEquals(0, stack.size());
        Assert.assertNull(stack.pop());
        Assert.assertEquals(0, stack.size());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        stack.push("5");
        stack.push("6");
        stack.push("7");
        stack.push("8");
        stack.push("9");
        stack.push("10");
        stack.push("11");
        stack.push("12");
        stack.push("13");
        stack.push("14");
        stack.push("15");
        int iterations = 0;
        for (String item : stack) {
            iterations++;
        }
        Assert.assertEquals(15, iterations);
        iterations = 0;
        for (String item : stack) {
            iterations++;
        }
        Assert.assertEquals(15, iterations);
        Assert.assertEquals(15, stack.size());
        Assert.assertFalse(stack.isEmpty());
        Assert.assertEquals("15", stack.peek());
        Assert.assertEquals("15", stack.pop());
        Assert.assertEquals("14", stack.pop());
        Assert.assertEquals("13", stack.pop());
        Assert.assertEquals("12", stack.pop());
        Assert.assertEquals("11", stack.pop());
        Assert.assertEquals("10", stack.pop());
        Assert.assertEquals("9", stack.pop());
        Assert.assertEquals("8", stack.pop());
        Assert.assertEquals("7", stack.pop());
        Assert.assertEquals("6", stack.pop());
        Assert.assertEquals("5", stack.pop());
        Assert.assertEquals("4", stack.pop());
        Assert.assertEquals(3, stack.size());
    }
}
