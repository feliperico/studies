package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Queue;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 22/10/15.
 */
public class LinkedListQueueTest {


    @Test
    public void testSeveralOperations() {
        Queue<String> queue= new LinkedListQueue<String>();
        Assert.assertTrue(queue.isEmpty());
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        Assert.assertFalse(queue.isEmpty());
        Assert.assertEquals(3, queue.size());
        Assert.assertEquals("1", queue.first());
        Assert.assertEquals(3, queue.size());
        Assert.assertEquals("1", queue.dequeue());
        Assert.assertEquals("2", queue.dequeue());
        Assert.assertEquals(1, queue.size());
        Assert.assertEquals("3", queue.dequeue());
        Assert.assertEquals(0, queue.size());
        Assert.assertNull(queue.dequeue());
        Assert.assertEquals(0, queue.size());
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        queue.enqueue("5");
        int iterations = 0;
        for (String item : queue) {
            iterations++;
        }
        Assert.assertEquals(5, iterations);
        iterations = 0;
        for (String item : queue) {
            iterations++;
        }
        Assert.assertEquals(5, iterations);
        Assert.assertEquals(5, queue.size());
        Assert.assertFalse(queue.isEmpty());
        Assert.assertEquals("1", queue.first());
    }
}
