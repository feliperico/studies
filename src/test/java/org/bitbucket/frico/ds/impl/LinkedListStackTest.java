package org.bitbucket.frico.ds.impl;

import org.bitbucket.frico.ds.Stack;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 21/10/15.
 */
public class LinkedListStackTest {

    @Test
    public void testSeveralOperations() {
        Stack<String> stack = new LinkedListStack<String>();
        Assert.assertTrue(stack.isEmpty());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        Assert.assertFalse(stack.isEmpty());
        Assert.assertEquals(3, stack.size());
        Assert.assertEquals("3", stack.peek());
        Assert.assertEquals(3, stack.size());
        Assert.assertEquals("3", stack.pop());
        Assert.assertEquals("2", stack.pop());
        Assert.assertEquals(1, stack.size());
        Assert.assertEquals("1", stack.pop());
        Assert.assertEquals(0, stack.size());
        Assert.assertNull(stack.pop());
        Assert.assertEquals(0, stack.size());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        stack.push("5");
        int iterations = 0;
        for (String item : stack) {
            iterations++;
        }
        Assert.assertEquals(5, iterations);
        iterations = 0;
        for (String item : stack) {
            iterations++;
        }
        Assert.assertEquals(5, iterations);
        Assert.assertEquals(5, stack.size());
        Assert.assertFalse(stack.isEmpty());
        Assert.assertEquals("5", stack.peek());
    }

}
