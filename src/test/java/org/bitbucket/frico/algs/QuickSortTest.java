package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 27/10/15.
 */
public class QuickSortTest {

    @Test
    public void test() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        Assert.assertFalse(QuickSort.isSorted(array));
        QuickSort.sort(array);
        Assert.assertTrue(QuickSort.isSorted(array));
    }

    @Test
    public void testSelection() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        int k = (Integer) QuickSort.selectKthElement(array, 10);
        Assert.assertEquals(9, k);
    }
}
