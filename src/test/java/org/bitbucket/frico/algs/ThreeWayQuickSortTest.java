package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 27/10/15.
 */
public class ThreeWayQuickSortTest {

    @Test
    public void test() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        Assert.assertFalse(ThreeWayQuickSort.isSorted(array));
        ThreeWayQuickSort.sort(array);
        Assert.assertTrue(ThreeWayQuickSort.isSorted(array));
    }
}
