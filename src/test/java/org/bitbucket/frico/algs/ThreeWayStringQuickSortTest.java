package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

public class ThreeWayStringQuickSortTest {

    @Test
    public void testSort() {
        String[] strings = new String[] {
                "DDBJQ9H58P", "Y2N2SBI5NK", "R2PZHKVV0R", "7AQ03N97OV",
                "UYTG6EDC3M", "NLMX8RJDPB", "HU9QMPY1XZ", "18G0EIQBKI",
                "76AEENL2CV", "OB1VCBQBGR", "ECNTQSAIH3", "HYE6VWSJVE",
                "ECAIH3", "ECNTQSB", "HU9QMPA"
        };
        String[] sortedStrings = new String[] {
                "18G0EIQBKI", "76AEENL2CV", "7AQ03N97OV", "DDBJQ9H58P",
                "ECAIH3", "ECNTQSAIH3", "ECNTQSB", "HU9QMPA", "HU9QMPY1XZ",
                "HYE6VWSJVE", "NLMX8RJDPB",
                "OB1VCBQBGR", "R2PZHKVV0R", "UYTG6EDC3M", "Y2N2SBI5NK"
        };

        strings = ThreeWayStringQuickSort.sort(strings);

        for (int i = 0; i < strings.length; i++) {
            Assert.assertEquals(strings[i], sortedStrings[i]);
        }
    }

}
