package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 03/11/15.
 */
public class SortingUtilsTest {

    @Test
    public void binarySearchTest() {
        Integer[] array = new Integer[] {8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        QuickSort.sort(array);
        Assert.assertTrue(SortingUtils.binarySearch(array, 9));
        Assert.assertTrue(SortingUtils.binarySearch(array, 8));
        Assert.assertTrue(SortingUtils.binarySearch(array, -7));
        Assert.assertTrue(SortingUtils.binarySearch(array, 18));
        Assert.assertTrue(SortingUtils.binarySearch(array, 110));
        Assert.assertFalse(SortingUtils.binarySearch(array, 22));
        Assert.assertFalse(SortingUtils.binarySearch(array, 23));
        Assert.assertFalse(SortingUtils.binarySearch(array, 24));
        Assert.assertFalse(SortingUtils.binarySearch(array, 111));

    }
}
