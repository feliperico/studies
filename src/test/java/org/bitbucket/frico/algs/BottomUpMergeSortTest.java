package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

/**
 * Created by feliperico on 26/10/15.
 */
public class BottomUpMergeSortTest {

    @Test
    public void test() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        Assert.assertFalse(BottomUpMergeSort.isSorted(array));
        BottomUpMergeSort.sort(array);
        Assert.assertTrue(BottomUpMergeSort.isSorted(array));
    }

    @Test
    public void testComparator() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10};
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        };
        Assert.assertFalse(BottomUpMergeSort.isSorted(array));
        BottomUpMergeSort.sort(array, comp);
        Assert.assertTrue(BottomUpMergeSort.isSorted(array));
    }
}
