package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

/**
 * Created by feliperico on 23/10/15.
 */
public class InsertionSortTest {

    @Test
    public void test() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0};
        Assert.assertFalse(InsertionSort.isSorted(array));
        InsertionSort.sort(array);
        Assert.assertTrue(InsertionSort.isSorted(array));
    }

    @Test
    public void testComparator() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0};
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        };
        Assert.assertFalse(InsertionSort.isSorted(array));
        InsertionSort.sort(array, comp);
        Assert.assertTrue(InsertionSort.isSorted(array));
    }
}
