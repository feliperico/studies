package org.bitbucket.frico.algs;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 29/10/15.
 */
public class HeapSortTest {

    @Test
    public void test() {
        Integer[] array = new Integer[] {10, 8, 4, 2, 6, 7, 9, 5, 1, 3, 0, 110, 18, 14, 12, 16, 17, 19, 15, 11, 13, 10, -7};
        Assert.assertFalse(HeapSort.isSorted(array));
        HeapSort.sort(array);
        Assert.assertTrue(HeapSort.isSorted(array));
    }

}
