package org.bitbucket.frico.exerc.random;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by feliperico on 22/10/15.
 */
public class ArithExpressionParsingAlgTest {

    @Test
    public void testArithmeticExpression1() {
        Assert.assertEquals(101d, ArithExpressionParsingAlg.calculateResult("( ( ( ( 5 + 1 ) / 3 ) * 50 ) + 1 )"), 0d);
        Assert.assertEquals(101d, ArithExpressionParsingAlg.calculateResult("( 1 + ( 50 * ( ( 5 + 1 ) / 3 ) ) )"), 0d);
    }
}
